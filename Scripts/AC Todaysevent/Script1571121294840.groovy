import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

'Opening SAAS site'
WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

'Keyin registered email address in the email field'
WebUI.setText(findTestObject('Object Repository/AC-TodaysEvent/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'lou.gill000+CRTESTSMK12@gmail.com')

'Keyin valid password in the Password field'
WebUI.setEncryptedText(findTestObject('Object Repository/AC-TodaysEvent/Page_HRdirect/input_PASSWORD_Password'), 'vTJxSJ71VSN3xk74LjKEvw==')

'Click Sign in to my account button'
WebUI.click(findTestObject('Object Repository/AC-TodaysEvent/Page_HRdirect/button_exit_to_app Sign In to'))

'Maximizing the bowser window'
WebUI.maximizeWindow()

'Launching Calendar application'
WebUI.click(findTestObject('AC-TodaysEvent/Page_HRdirect/AttendanceCalendar_App'))

WebUI.delay(20)

'Clicking All Employee dropdown'
WebUI.click(findTestObject('Object Repository/AC-TodaysEvent/Page_HRdirect/div_All Active Employees'))

WebUI.delay(3)

'Selecting Jorge Mcfadden from the list of employee'
WebUI.click(findTestObject('Object Repository/AC-TodaysEvent/Page_HRdirect/div_Mcfadden Jorge'))

WebUI.delay(3)

'Clicking Add event button'
WebUI.click(findTestObject('Object Repository/AC-TodaysEvent/Page_HRdirect/button_Add Event'))

WebUI.delay(3)

'Clicking Absence Code dropdown'
WebUI.click(findTestObject('AC-TodaysEvent/Page_HRdirect/AbsenceCode_dropdown'))

WebUI.delay(3)

'Selecting Partial Hours Worked from the list of Absence Code'
WebUI.click(findTestObject('AC-TodaysEvent/Page_HRdirect/AbsenceCode_PartialHoursWorked'))

WebUI.delay(3)

'Selecting NONE from the list '
WebUI.selectOptionByValue(findTestObject('Object Repository/AC-TodaysEvent/Page_HRdirect/select_VacationSickNone'), 'D100000', 
    true)

WebUI.delay(3)

'Setting 6 hours in Time field'
WebUI.setText(findTestObject('Object Repository/AC-TodaysEvent/Page_HRdirect/input_Time_timeTaken'), '6')

'Saving the Event'
WebUI.click(findTestObject('Object Repository/AC-TodaysEvent/Page_HRdirect/button_Save'))

WebUI.delay(3)

'Then newly added event appears under Today\'s Event list'
WebUI.verifyElementVisible(findTestObject('AC-TodaysEvent/Page_HRdirect/TodaysEvent_Jorge'))

WebUI.delay(3)

'Clicking Profile icon'
WebUI.click(findTestObject('Object Repository/AC-TodaysEvent/Page_HRdirect/i_arrow_drop_down'))

WebUI.delay(3)

'Clicking Sign out button'
WebUI.click(findTestObject('Object Repository/AC-TodaysEvent/Page_HRdirect/i_exit_to_app'))

WebUI.delay(5)

'Verifying logout is successful by checking Email field is visisble in the page'
WebUI.verifyElementPresent(findTestObject('AC-TodaysEvent/Page_HRdirect/input_EMAIL ADDRESS_Email'), 0)

WebUI.delay(3)

'Closing the browser after the test'
WebUI.closeBrowser()

