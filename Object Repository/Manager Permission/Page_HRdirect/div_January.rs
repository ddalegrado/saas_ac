<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_January</name>
   <tag></tag>
   <elementGuidId>8dbc29ab-66ad-441d-8de4-41a800745be9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//platform-apps-main[@id='wrapper']/div/ng-component/div/div[2]/calendar-component/div[3]/calendar-year-component/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>year-view</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
                January
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            30
                        
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                
            
                
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                
            
                
                    
                        
                            13
                        
                    
                        
                            14
                        
                    
                        
                            15
                        
                    
                        
                            16
                        
                    
                        
                            17
                        
                    
                        
                            18
                        
                    
                        
                            19
                        
                    
                
            
                
                    
                        
                            20
                        
                    
                        
                            21
                        
                    
                        
                            22
                        
                    
                        
                            23
                        
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                
            
                
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                
            
                
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                
            
        
    
        
            
                February
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                
            
                
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                
            
                
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                        
                            13
                        
                    
                        
                            14
                        
                    
                        
                            15
                        
                    
                        
                            16
                        
                    
                
            
                
                    
                        
                            17
                        
                    
                        
                            18
                        
                    
                        
                            19
                        
                    
                        
                            20
                        
                    
                        
                            21
                        
                    
                        
                            22
                        
                    
                        
                            23
                        
                    
                
            
                
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                
            
                
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                
            
        
    
        
            
                March
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                
            
                
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                
            
                
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                        
                            13
                        
                    
                        
                            14
                        
                    
                        
                            15
                        
                    
                        
                            16
                        
                    
                
            
                
                    
                        
                            17
                        
                    
                        
                            18
                        
                    
                        
                            19
                        
                    
                        
                            20
                        
                    
                        
                            21
                        
                    
                        
                            22
                        
                    
                        
                            23
                        
                    
                
            
                
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                
            
                
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                
            
        
    
        
            
                April
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                
            
                
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                        
                            13
                        
                    
                
            
                
                    
                        
                            14
                        
                    
                        
                            15
                        
                    
                        
                            16
                        
                    
                        
                            17
                        
                    
                        
                            18
                        
                    
                        
                            19
                        
                    
                        
                            20
                        
                    
                
            
                
                    
                        
                            21
                        
                    
                        
                            22
                        
                    
                        
                            23
                        
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                
            
                
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                
            
                
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                
            
        
    
        
            
                May
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                
            
                
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                
            
                
                    
                        
                            12
                        
                    
                        
                            13
                        
                    
                        
                            14
                        
                    
                        
                            15
                        
                    
                        
                            16
                        
                    
                        
                            17
                        
                    
                        
                            18
                        
                    
                
            
                
                    
                        
                            19
                        
                    
                        
                            20
                        
                    
                        
                            21
                        
                    
                        
                            22
                        
                    
                        
                            23
                        
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                
            
                
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                
            
                
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                
            
        
    
        
            
                June
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                
            
                
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                
            
                
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                        
                            13
                        
                    
                        
                            14
                        
                    
                        
                            15
                        
                    
                
            
                
                    
                        
                            16
                        
                    
                        
                            17
                        
                    
                        
                            18
                        
                    
                        
                            19
                        
                    
                        
                            20
                        
                    
                        
                            21
                        
                    
                        
                            22
                        
                    
                
            
                
                    
                        
                            23
                        
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                
            
                
                    
                        
                            30
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                
            
        
    
        
            
                July
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            30
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                
            
                
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                        
                            13
                        
                    
                
            
                
                    
                        
                            14
                        
                    
                        
                            15
                        
                    
                        
                            16
                        
                    
                        
                            17
                        
                    
                        
                            18
                        
                    
                        
                            19
                        
                    
                        
                            20
                        
                    
                
            
                
                    
                        
                            21
                        
                    
                        
                            22
                        
                    
                        
                            23
                        
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                
            
                
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                
            
                
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                
            
        
    
        
            
                August
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                
            
                
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                
            
                
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                        
                            13
                        
                    
                        
                            14
                        
                    
                        
                            15
                        
                    
                        
                            16
                        
                    
                        
                            17
                        
                    
                
            
                
                    
                        
                            18
                        
                    
                        
                            19
                        
                    
                        
                            20
                        
                    
                        
                            21
                        
                    
                        
                            22
                        
                    
                        
                            23
                        
                    
                        
                            24
                        
                    
                
            
                
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            31
                        
                    
                
            
                
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                
            
        
    
        
            
                September
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                
            
                
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                        
                            13
                        
                    
                        
                            14
                        
                    
                
            
                
                    
                        
                            15
                        
                    
                        
                            16
                        
                    
                        
                            17
                        
                    
                        
                            18
                        
                    
                        
                            19
                        
                    
                        
                            20
                        
                    
                        
                            21
                        
                    
                
            
                
                    
                        
                            22
                        
                    
                        
                            23
                        
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                
            
                
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                
            
                
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                
            
        
    
        
            
                October
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                
            
                
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                
            
                
                    
                        
                            13
                        
                    
                        
                            14
                        
                    
                        
                            15
                        
                    
                        
                            16
                        
                    
                        
                            17
                        
                    
                        
                            18
                        
                    
                        
                            19
                        
                    
                
            
                
                    
                        
                            20
                        
                    
                        
                            21
                        
                    
                        
                            22
                        
                    
                        
                            23
                        
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                
            
                
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                
            
                
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                
            
        
    
        
            
                November
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                
            
                
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                
            
                
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                        
                            13
                        
                    
                        
                            14
                        
                    
                        
                            15
                        
                    
                        
                            16
                        
                    
                
            
                
                    
                        
                            17
                        
                    
                        
                            18
                        
                    
                        
                            19
                        
                    
                        
                            20
                        
                    
                        
                            21
                        
                    
                        
                            22
                        
                    
                        
                            23
                        
                    
                
            
                
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                
            
                
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                
            
        
    
        
            
                December
            
            
                
                    S
                
                
                    M
                
                
                    T
                
                
                    W
                
                
                    T
                
                
                    F
                
                
                    S
                
            
            
                
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                
            
                
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                        
                            12
                        
                    
                        
                            13
                        
                    
                        
                            14
                        
                    
                
            
                
                    
                        
                            15
                        
                    
                        
                            16
                        
                    
                        
                            17
                        
                    
                        
                            18
                        
                    
                        
                            19
                        
                    
                        
                            20
                        
                    
                        
                            21
                        
                    
                
            
                
                    
                        
                            22
                        
                    
                        
                            23
                        
                    
                        
                            24
                        
                    
                        
                            25
                        
                    
                        
                            26
                        
                    
                        
                            27
                        
                    
                        
                            28
                        
                    
                
            
                
                    
                        
                            29
                        
                    
                        
                            30
                        
                    
                        
                            31
                        
                    
                        
                            1
                        
                    
                        
                            2
                        
                    
                        
                            3
                        
                    
                        
                            4
                        
                    
                
            
                
                    
                        
                            5
                        
                    
                        
                            6
                        
                    
                        
                            7
                        
                    
                        
                            8
                        
                    
                        
                            9
                        
                    
                        
                            10
                        
                    
                        
                            11
                        
                    
                
            
        
    
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wrapper&quot;)/div[@class=&quot;content-main-scroller&quot;]/ng-component[1]/div[@class=&quot;main&quot;]/div[@class=&quot;content&quot;]/calendar-component[1]/div[@class=&quot;calendar&quot;]/calendar-year-component[1]/div[@class=&quot;year-view&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//platform-apps-main[@id='wrapper']/div/ng-component/div/div[2]/calendar-component/div[3]/calendar-year-component/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='keyboard_arrow_right'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='keyboard_arrow_left'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//calendar-year-component/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
