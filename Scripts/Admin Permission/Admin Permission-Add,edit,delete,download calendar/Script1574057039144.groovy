import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.openqa.selenium.Keys as Keys

def t1 = new Thread((({ 
        for (i = 0; i < 10; i++) {
            Robot robot = new Robot()

            Thread.sleep(5000)

            robot.keyPress(KeyEvent.VK_ENTER)

            Thread.sleep(1000)

            break
        }
    }) as Runnable))

WebUI.openBrowser('')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.setText(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/input_EMAIL ADDRESS_Email'), 
    'lou.gill000+CRTESTAC4ad@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/input_PASSWORD_Password'), 
    'vTJxSJ71VSN3xk74LjKEvw==')

WebUI.sendKeys(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/input_PASSWORD_Password'), 
    Keys.chord(Keys.ENTER))

WebUI.delay(10)

WebUI.doubleClick(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/a'))

WebUI.delay(20)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/div_All Active Employees'))

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/div_HY                                     _f8edca'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/span_12'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/div_Select a code'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/a_I                                        _8ab60f'))

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/select_VacationSickNone'), 
    'D673935', true)

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/input_Time_timeTaken'), 
    '12')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/span_I'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/div_HY                                     _690384'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/input__datePickerComponent'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/a_13'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/i_more_vert'))

WebUI.delay(3)

t1.start()

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/a_Download'))

//WebUI.switchToDefaultContent()
WebUI.delay(25)

WebUI.mouseOver(findTestObject('Admin Permission/Add-edit-download-delete/Page_HRdirect/span_12'))

WebUI.click(findTestObject('Admin Permission/Add-edit-download-delete/Page_HRdirect/span_12'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/div_HY                                     _690384'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/span_Delete Event'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/button_Yes'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/span_Raymond'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Admin Permission/Add-edit-download-delete/Page_HRdirect/span_Sign Out'))

WebUI.closeBrowser()

