import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

'Execute New Account '
WebUI.callTestCase(findTestCase('Create New Account'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Click Learn More on Attendance Calendar'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/LearnMore_AC2'))

WebUI.delay(2)

'Click on Get Started'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Get Started'))

'Input CC details'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input__address1'), '123 Main Dove Street')

WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input__city'), 'Tampa')

WebUI.selectOptionByValue(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/select_State'), 'FL', true)

WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input__postal_code'), '33013')

WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input_CC number'), '4012 8888 8888 1881')

WebUI.delay(2)

WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input_Month'), '12')

WebUI.delay(3)

WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input_Year'), '2023')

WebUI.delay(2)

WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input_CVV'), '123')

WebUI.delay(2)

'Click Pay Now'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Pay Now'))

WebUI.delay(75)

WebUI.delay(2)

'Click NEXT on Modal'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Next'))

WebUI.delay(2)

'Click Next'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Next_2'))

WebUI.delay(2)

WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/span_Yes'))

WebUI.delay(2)

WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Next_2'))

WebUI.delay(2)

'Click Complete'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Complete'))

WebUI.delay(30)

WebUI.delay(2)

'Go back to the Dashboard'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/Dashboard'))

WebUI.delay(2)

'Click TOR'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/select_TOR'))

WebUI.delay(2)

'Click Get Started'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Get Started_TOR'))

WebUI.delay(2)

WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Pay Now'))

'Get Started on Time Off Request'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Get Started_TOR_First Launch'))

WebUI.delay(2)

'Go to TOR Settings'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/TOR_Settings'))

'Click ADD code'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Add Code'))

'Input custom code'
WebUI.setText(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/input_Custom Code'), 
    'AB')

'Enter description'
WebUI.setText(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/input_absenceDescription'), 
    'Custom code -TOR')

'Click SAVE'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Save'))

WebUI.delay(3)

'Get value of Newly Added custom code in TOR'
New_Code_TOR = WebUI.getText(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/div_Custom code -TOR'))

WebUI.delay(2)

WebUI.delay(5)

'Go back to the Dashboard'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/img'))

WebUI.delay(2)

'Launch AC app'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/a_Calendar'))

WebUI.delay(25)

'Go to Settings tab'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/a_Settings'))

WebUI.delay(2)

'Click on the Custom Code edited in TOR'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/span_Custom Code - edited on TOR'))

'Get the TEXT value of Custom Code created in TOR'
VerifyCodeinAC = WebUI.getText(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/CheckAddedCode_inAC'))

'Verify that the code created in TOR is equal to the code shown in AC app'
WebUI.verifyMatch(New_Code_TOR, VerifyCodeinAC, false)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/span_Custom code -TOR'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/a_Calendar_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/div_All Active Employees'))

WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/div_Owner Owner'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/div_3'))

WebUI.delay(2)

WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/Scroll_AbsenceCode_AC'))

WebUI.delay(2)

WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/Select_AbsenceCode_AC'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Save_1'))

WebUI.delay(5)

'Go back to the Dashboard'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/Dashboard'))

WebUI.delay(2)

'Select TOR'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/a_TOR'))

WebUI.delay(2)

'-----TO BE REMOVED ONCE ISSUE IS FIXED----------'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Get Started'))

WebUI.delay(2)

'Go to TOR Settings'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/TOR_Settings'))

WebUI.delay(2)

'click option'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/i_more_vert'))

'select edit'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/a_Edit'))

WebUI.delay(2)

'edit description code'
WebUI.setText(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/input_editDescription_TOR'), 'Custom Code - edited on TOR')

WebUI.delay(2)

'Save'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Save'))

WebUI.delay(2)

TOR_CodeDesc = WebUI.getText(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/TOR_EditedDesc'))

WebUI.delay(2)

'Go back to the Dashboard'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/img'))

WebUI.delay(2)

'Launch AC app'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/a_Calendar'))

WebUI.delay(25)

'Go to Settings tab'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/a_Settings'))

WebUI.delay(2)

'Click on the Custom Code edited in TOR'
WebUI.click(findTestObject('Object Repository/Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/span_Custom Code - edited on TOR'))

AC_EditCodeDesc = WebUI.getText(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/AC_CustomCode_EditedDesc'))

WebUI.verifyMatch(AC_EditCodeDesc, TOR_CodeDesc, false)

