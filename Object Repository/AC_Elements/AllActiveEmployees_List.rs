<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AllActiveEmployees_List</name>
   <tag></tag>
   <elementGuidId>8ec934af-8f68-4875-8c4f-0484168714b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='employee-list']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-collapse-menu collapse in</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>employee-list</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
            
            
                
                    search
                    
                
            
            

            
            Active
            
            
                
                    
                
                
                    All Active Employees
                
            
            
                
                    
                        
                        
                        
                        
                            CA
                        
                    
                    
                        Admin, Cherish
                    
                
            
                
                    
                        
                        
                        
                        
                            KE
                        
                    
                    
                        Employee1, Kaylynn
                    
                
            
                
                    
                        
                        
                        
                        
                            KE
                        
                    
                    
                        Employee10, Karley
                    
                
            
                
                    
                        
                        
                        
                        
                            LE
                        
                    
                    
                        Employee2, Leslie
                    
                
            
                
                    
                        
                        
                        
                        
                            ME
                        
                    
                    
                        Employee3, Mina
                    
                
            
                
                    
                        
                        
                        
                        
                            CE
                        
                    
                    
                        Employee4, Carissa
                    
                
            
                
                    
                        
                        
                        
                        
                            ME
                        
                    
                    
                        Employee5, Maribel
                    
                
            
                
                    
                        
                        
                        
                        
                            JE
                        
                    
                    
                        Employee6, Jane
                    
                
            
                
                    
                        
                        
                        
                        
                            LE
                        
                    
                    
                        Employee7, Lexie
                    
                
            
                
                    
                        
                        
                        
                        
                            ME
                        
                    
                    
                        Employee8, Makenzie
                    
                
            
                
                    
                        
                        
                        
                        
                            AE
                        
                    
                    
                        Employee9, Amy
                    
                
            
                
                    
                        
                        
                        
                        
                            SM
                        
                    
                    
                        Manager, Skye
                    
                
            
                
                    
                        
                        
                        
                        
                            FS
                        
                    
                    
                        Slade, Freddie
                    
                
            

            
            
            
            
            

        
    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;employee-list&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='employee-list']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//platform-apps-main[@id='wrapper']/div/ng-component/div/div/employee-list-search-component/div/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Active Employees'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings'])[2]/following::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//employee-list-search-component/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
