<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Total Time Requested</name>
   <tag></tag>
   <elementGuidId>9d279bd2-d16e-4ff8-b2e3-e81f9703da7c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='calendarquestionnaireform']/div/div/div[4]/calendar-detail-component/div/div/div/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Total Time Requested: </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;calendarquestionnaireform&quot;)/div[@class=&quot;row noPadding&quot;]/div[@class=&quot;col-xs-12 col-sm-12&quot;]/div[@class=&quot;calendar-detail-view&quot;]/calendar-detail-component[1]/div[@class=&quot;request-details&quot;]/div[@class=&quot;row noPadding&quot;]/div[@class=&quot;col-xs-12 col-sm-6 pull-left&quot;]/div[@class=&quot;total form-group&quot;]/label[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='calendarquestionnaireform']/div/div/div[4]/calendar-detail-component/div/div/div/div/label</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[9]/following::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[10]/preceding::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//calendar-detail-component/div/div/div/div/label</value>
   </webElementXpaths>
</WebElementEntity>
