import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/input_EMAIL ADDRESS'), 
    'halebmerin+38saas19qa@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/input_PASSWORD'), 
    'cHBFEg1ykROLjuIVhbxYXA==')

WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/button_Sign In to My Account'))

WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/app_AC'))

WebUI.delay(20)

'Click Employee list dropdown'
WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/div_All Active Employees'))

WebUI.delay(2)

'Select employe = Admin Admin'
WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/div_Admin Admin'))

WebUI.delay(2)

'Click Add Event'
WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/button_Add Event'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/div_Select a code'))

WebUI.delay(2)

'Select Absence Code'
WebUI.click(findTestObject('Transfer event from current year to next year/Page_HRdirect/AbsentCode_Additional Hrs'))

WebUI.delay(2)

'Enter time'
WebUI.setText(findTestObject('Transfer event from current year to next year/Page_HRdirect/input_Time_Taken'), '2')

'Add notes'
WebUI.setText(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/textarea_Notes_notes'), 
    'Create new event')

WebUI.delay(2)

'Save Event'
WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/button_Save'))

WebUI.delay(2)

'Select Created event'
WebUI.click(findTestObject('Transfer event from current year to next year/Page_HRdirect/CreatedEvent'))

WebUI.delay(2)

'Click Employee name - Admin Admin'
WebUI.click(findTestObject('Transfer event from current year to next year/Page_HRdirect/div_EmployeeWithEvent'))

WebUI.delay(2)

'Edit date and input year (next year)'
WebUI.setText(findTestObject('Transfer event from current year to next year/Page_HRdirect/input__datePickerComponent'), 
    '02/12/2021')

WebUI.delay(2)

'Add note'
WebUI.setText(findTestObject('Transfer event from current year to next year/Page_HRdirect/textarea_Notes_notes'), 'Move to Next Year')

WebUI.delay(2)

'Click Save'
WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/button_Save'))

WebUI.delay(2)

'Click Arrow - to move to the next year'
WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/i_keyboard_arrow_right'))

WebUI.delay(2)

'Select Date with moved event'
WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/span_A'))

WebUI.click(findTestObject('Transfer event from current year to next year/Page_HRdirect/div_EmployeeWithEvent'))

WebUI.delay(2)

'Check Date / Year'
not_run: WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/input__datePickerComponent'))

WebUI.delay(2)

'Delete Event'
WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/span_Delete Event'))

WebUI.delay(5)

'Confirm Delete'
WebUI.click(findTestObject('Object Repository/Transfer event from current year to next year/Page_HRdirect/button_Yes'))

WebUI.delay(2)

