<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Company_Industry</name>
   <tag></tag>
   <elementGuidId>38ad0249-0c42-4fe7-869b-d0e68851aa10</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>CompanyIndustry</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>CompanyIndustry</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                    Please select an appropriate industry
                                                        Call Center
                                                        Construction
                                                        Education
                                                        Fleet
                                                        Health Club/Gym
                                                        Lawn &amp; Landscaping
                                                        Manufacturing
                                                        Medical
                                                        Professional
                                                        Restaurant/Bar
                                                        Retail
                                                        Security Guard
                                                        Service Delivery
                                                        Social Service
                                                        Spa/Salon
                                                        Volunteer
                                                        Worship
                                                        Other
                                                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;CompanyIndustry&quot;)</value>
   </webElementProperties>
</WebElementEntity>
