<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_I</name>
   <tag></tag>
   <elementGuidId>2f8e70e6-d90c-477b-8dce-161fcb3d2268</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//platform-apps-main[@id='wrapper']/div/ng-component/div/div[2]/calendar-component/div[3]/calendar-year-component/div[2]/div[2]/div/div[5]/div/div[3]/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>code</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    I
                                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wrapper&quot;)/div[@class=&quot;content-main-scroller&quot;]/ng-component[1]/div[@class=&quot;main&quot;]/div[@class=&quot;content&quot;]/calendar-component[1]/div[@class=&quot;calendar&quot;]/calendar-year-component[1]/div[@class=&quot;year-view&quot;]/div[@class=&quot;month-box&quot;]/div[@class=&quot;month&quot;]/div[@class=&quot;monthdays&quot;]/div[@class=&quot;week&quot;]/div[@class=&quot;day option event&quot;]/div[@class=&quot;event-area bottom&quot;]/div[@class=&quot;event-code&quot;]/span[@class=&quot;code&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//platform-apps-main[@id='wrapper']/div/ng-component/div/div[2]/calendar-component/div[3]/calendar-year-component/div[2]/div[2]/div/div[5]/div/div[3]/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='S'])[6]/following::span[17]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='F'])[3]/following::span[18]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='March'])[1]/preceding::span[27]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='S'])[7]/preceding::span[28]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='I']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div[3]/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
