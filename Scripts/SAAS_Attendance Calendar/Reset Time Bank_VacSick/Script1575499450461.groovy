import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://account.hrdirectappsqa.com/')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'donatest1+SAAS384@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/input_PASSWORD_Password'), 
    '3Z9vEXbPUsB23Jtp1+ckNg==')

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_exit_to_app Sign In to My Account'))

WebUI.delay(10)

'Launching Attendance Calendar app'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/a'))

WebUI.delay(10)

not_run: WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_Attendance-Calendar-IVpdf - HRdirect/a_Try Beta'))

'Click All Employee dropdown'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/div_All Active Employees'))

WebUI.delay(3)

'Select employee name "Four Four"'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/div_FF                                                                                                        Four Four (1)'))

WebUI.delay(3)

not_run: WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_year'))

'Click Month toggle'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/Month_toggle'))

WebUI.delay(3)

'Set Vacation Time bank start time to 100'
WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (1)'), '100')

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (1)'))

'Click Save button'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save'))

WebUI.delay(5)

'Set Sick Time bank start time to 100'
WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (2)'), '100')

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (2)'))

'Click Save button'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save'))

WebUI.delay(10)

'Click Add Event button'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/button_Add Event'))

WebUI.delay(5)

'Click Absence Code dropdown'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/div_Select a code'))

'Select Leave of Absence'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Leave of Absence'))

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/select_Select a code'), 
    '1179', true)

'Set the date'
WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input__datePickerComponent'), '02/06/2020')

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Time_timeTaken'))

'Set time value'
WebUI.setText(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/input_Time_timeTaken'), '10')

'Click Save button'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save_1'))

WebUI.delay(10)

'Click Add Event button'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/button_Add Event'))

WebUI.delay(5)

'Click Absence Code dropdown'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/div_Select a code'))

'Select Illness-Self'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Illness-Self (1)'))

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/select_VacationSickNone'), 
    'D673935', true)

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/select_Select a code'), 
    '1176', true)

not_run: WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input__datePickerComponent'), '10/04/2019')

'Click Time Bank dropdown'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/select_VacationSickNone'))

'Select Sick'
WebUI.selectOptionByLabel(findTestObject('Settings_VacSickToPTO/Page_HRdirect/select_VacationSickNone'), 'Sick', true)

'Set the date'
WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input__datePickerComponent'), '02/04/2020')

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Time_timeTaken'))

'Set time value'
WebUI.setText(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/input_Time_timeTaken'), '5')

'Click Save button'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save_2'))

WebUI.delay(10)

'Click Settings tab'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/a_Settings (1)'))

WebUI.delay(5)

'Click PTO radio button'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/span_PTO'))

WebUI.delay(5)

'Click Save button'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save_3'))

WebUI.delay(5)

'Click Calendar tab'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/a_Calendar (1)'))

WebUI.delay(5)

'Click All Employee dropdown'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/div_All Active Employees'))

WebUI.delay(3)

'Select employee name "Four Four"'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/div_FF                                                                                                        Four Four (1)'))

WebUI.delay(3)

'Click Month toggle'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/Month_toggle'))

not_run: WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/a_Month'))

WebUI.delay(5)

WebUI.verifyElementPresent(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Leave of Absence (3)'), 5)

'Click Leave of Absence event from the calendar'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Leave of Absence (3)'))

WebUI.delay(3)

'Click event from the list'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_four (1)'))

WebUI.delay(5)

'Click Delete'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/span_Delete Event'))

WebUI.delay(5)

'Click Yes button from the confirmation modal'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Yes'))

WebUI.delay(10)

WebUI.verifyElementPresent(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Illness-Self (4)'), 3)

'Click Illness-Self event from the calendar'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Illness-Self (4)'))

WebUI.delay(3)

'Click event from the list'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_four (1)'))

WebUI.delay(5)

'Click Delete'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/span_Delete Event'))

WebUI.delay(5)

'Click Yes button from the confirmation modal'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Yes'))

WebUI.delay(10)

'Set PTO Time bank start time to 0'
WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (1)'), '0')

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (1)'))

'Click Save button'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save'))

WebUI.delay(10)

'Click Settings tab'
WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/a_Settings (1)'))

WebUI.delay(10)

'Click Vacation & Sick radio button'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/span_Vacation  Sick'))

'Click Save button'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save_4'))

WebUI.delay(10)

'Click profile icon'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/i_arrow_drop_down'))

'Click Sign out'
WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/span_Sign Out'))

WebUI.closeBrowser()

