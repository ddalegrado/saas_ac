import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://account.hrdirectappsqa.com/')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'donatest1+SAAS383@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/input_PASSWORD_Password'), 
    '3Z9vEXbPUsB23Jtp1+ckNg==')

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/button_exit_to_app Sign In to My Account'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/a'))

not_run: WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_Attendance-Calendar-IVpdf - HRdirect/a_Try Beta'))

WebUI.delay(20)

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/div_All Active Employees'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/div_Delgado Byron (1)'))

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/Month_toggle'))

not_run: WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/button_year'))

not_run: WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/a_Month'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/input_Starting Hours_vacationStartTime'), 
    '100')

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/input_Starting Hours_vacationStartTime'))

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/button_Save'))

WebUI.delay(10)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/button_Add Event'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/span_Select a code'))

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/span_Additional Hours (1)'))

WebUI.setText(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/input__datePickerComponent'), '01/09/2020')

WebUI.setText(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/input_Time_timeTaken'), '10')

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/button_Save_1'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/button_Add Event'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/span_Select a code'))

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/span_Partial Hours Worked (1)'))

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/select_VacationSickNone'))

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/select_PTONone'))

WebUI.selectOptionByValue(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/select_PTONone'), 'D100000', true)

WebUI.setText(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/input__datePickerComponent'), '01/10/2020')

WebUI.setText(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/input_Time_timeTaken'), '5')

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/button_Save_1'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/a_Settings'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/span_Vacation  Sick'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/button_Save_2'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/a_Calendar'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/div_All Active Employees'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/div_Delgado Byron (1)'))

not_run: WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/button_year'))

not_run: WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/a_Month'))

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/Month_toggle'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/button_Add Event'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/span_Select a code'))

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/AbsenceCode_Excused'))

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/select_VacationSickNone'))

WebUI.setText(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/input__datePickerComponent'), '01/17/2020')

WebUI.setText(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/input_Time_timeTaken'), '10')

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/button_Save_3'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/Month_toggle'))

not_run: WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/i_keyboard_arrow_left'))

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/Sched_Event_date'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/EventList'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/span_Delete Event'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/button_Yes'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/Month_toggle'))

WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/span_Partial Hours Worked'), 0)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/span_Partial Hours Worked'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/div_BD'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/span_Delete Event (1)'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/button_Yes'))

WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/Event_Excused'), 0)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/Event_Excused'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/div_BD (1)'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/span_Delete Event (1)'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/button_Yes (1)'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/input_Starting Hours_vacationStartTime (1)'))

WebUI.setText(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/input_Starting Hours_vacationStartTime (1)'), '0')

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/a_Settings'))

WebUI.delay(10)

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/span_PTO'))

WebUI.click(findTestObject('Settings_PTOtoVacSick/Page_HRdirect/button_Save_2'))

WebUI.delay(10)

not_run: WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/i_keyboard_arrow_right'))

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/i_arrow_drop_down'))

WebUI.click(findTestObject('Object Repository/Settings_PTOtoVacSick/Page_HRdirect/span_Sign Out'))

WebUI.closeBrowser()

