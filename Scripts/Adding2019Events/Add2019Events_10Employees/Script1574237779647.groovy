import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('account.hrdirectapps.com')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Add2019Events/input_EMAIL ADDRESS_Email'), 'tstestp1+11crtest@gmail.com')

WebUI.setText(findTestObject('Add2019Events/input_PASSWORD_Password'), 'Password_3')

WebUI.click(findTestObject('Add2019Events/button_exit_to_app Sign In to My Account'))

WebUI.delay(10)

WebUI.click(findTestObject('Add2019Events/a_AttendanceCalendar'))

WebUI.delay(20)

WebUI.click(findTestObject('Add2019Events/div_All Active Employees'))

WebUI.click(findTestObject('Add2019Events/div_Attitude One'))

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jan2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Feb2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Mar2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Apr2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_May2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jun2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jul2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Aug2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Sept2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Oct2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Nov2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Dec2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.click(findTestObject('Add2019Events/div_Attitude One'))

WebUI.click(findTestObject('Add2019Events/div_Beauty Two'))

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jan2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Feb2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Mar2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Apr2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_May2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jun2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jul2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Aug2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Sept2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Oct2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Nov2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Dec2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.click(findTestObject('Add2019Events/div_Beauty Two'))

WebUI.click(findTestObject('Add2019Events/div_Care Three'))

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jan2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Feb2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Mar2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Apr2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_May2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jun2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jul2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Aug2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Sept2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Oct2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Nov2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Dec2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.click(findTestObject('Add2019Events/div_Care Three'))

WebUI.click(findTestObject('Add2019Events/div_Discipline Four'))

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jan2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Feb2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Mar2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Apr2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_May2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jun2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jul2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Aug2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Sept2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Oct2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Nov2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Dec2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.click(findTestObject('Add2019Events/div_Discipline Four'))

WebUI.click(findTestObject('Add2019Events/div_Fate Five'))

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jan2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Feb2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Mar2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Apr2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_May2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jun2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jul2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Aug2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Sept2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Oct2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Nov2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Dec2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.click(findTestObject('Add2019Events/div_Fate Five'))

WebUI.click(findTestObject('Add2019Events/div_Gratitude Six'))

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jan2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Feb2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Mar2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Apr2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_May2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jun2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jul2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Aug2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Sept2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Oct2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Nov2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Dec2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.click(findTestObject('Add2019Events/div_Gratitude Six'))

WebUI.click(findTestObject('Add2019Events/div_Heal Seven'))

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jan2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Feb2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Mar2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Apr2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_May2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jun2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jul2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Aug2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Sept2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Oct2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Nov2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Dec2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.click(findTestObject('Add2019Events/div_Heal Seven'))

WebUI.click(findTestObject('Add2019Events/div_8 Eight'))

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jan2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Feb2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Mar2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Apr2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_May2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jun2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jul2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Aug2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Sept2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Oct2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Nov2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Dec2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.click(findTestObject('Add2019Events/div_8 Eight'))

WebUI.click(findTestObject('Add2019Events/div_9 Nine'))

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jan2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Feb2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Mar2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Apr2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_May2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jun2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jul2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Aug2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Sept2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Oct2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Nov2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Dec2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.click(findTestObject('Add2019Events/div_9 Nine'))

WebUI.click(findTestObject('Add2019Events/div_10 Ten'))

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jan2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Feb2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Mar2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Apr2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_May2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jun2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Jul2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Aug2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Sept2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Oct2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Nov2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Adding2019Events/AddingEvents_Dec2019'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.click(findTestObject('Add2019Events/i_arrow_drop_down'))

WebUI.click(findTestObject('Add2019Events/span_Sign Out'))

