import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

'Navigate to site'
WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

'Input an email of a registered account'
WebUI.setText(findTestObject('Resources_Download Blank Calendars/input_EMAIL ADDRESS_Email'), 'tstpqa+1ACQA_crtest@gmail.com')

'input password'
WebUI.setText(findTestObject('Resources_Download Blank Calendars/input_PASSWORD_Password'), 'a111111!')

WebUI.click(findTestObject('Resources_Download Blank Calendars/button_exit_to_app Sign In to My Account'))

WebUI.delay(5)

'Go to Admin Site'
WebUI.navigateToUrl('https://admin.hrdirectappsqa.com/customerlist')

WebUI.delay(5)

'Click on New Account'
WebUI.click(findTestObject('Add_AC_AdminPage/button_NewAccount'))

WebUI.delay(2)

'Input First Name'
WebUI.setText(findTestObject('Add_AC_AdminPage/Input_FirstName'), 'Owner')

WebUI.delay(2)

'Input Last Name'
WebUI.setText(findTestObject('Add_AC_AdminPage/Input_LastName'), 'Owner')

WebUI.delay(2)

'Get current date and time'
time = new Date().getTime()

'Use date and time with the email address'
Email = (('qa' + time) + '_crtest@yopmail.com')

'enter email address'
WebUI.setText(findTestObject('Add_AC_AdminPage/Input_Email'), Email)

WebUI.delay(2)

'Enter Company name'
WebUI.setText(findTestObject('Add_AC_AdminPage/Input_CompName'), 'Admin Added Comp')

WebUI.delay(2)

'Check \'Is Test\' box'
WebUI.click(findTestObject('Add_AC_AdminPage/check_IsTest'))

WebUI.delay(2)

'Enter phone number'
WebUI.setText(findTestObject('Add_AC_AdminPage/Input_Phone'), '98765432100')

WebUI.delay(2)

'Click Save button'
WebUI.click(findTestObject('Add_AC_AdminPage/button_SaveNewAccount'))

WebUI.delay(4)

not_run: WebUI.click(findTestObject('Add_AC_AdminPage/div_Owner owner'))

'Go to the Description tab'
WebUI.click(findTestObject('Add_AC_AdminPage/tab_Subscription'))

WebUI.delay(2)

'Click New Subscription'
WebUI.click(findTestObject('Add_AC_AdminPage/button_add_ New_Subsc'))

WebUI.delay(5)

'Click Add application dropdown'
WebUI.click(findTestObject('Add_AC_AdminPage/button_add_New_Apps'))

WebUI.delay(3)

WebUI.click(findTestObject('Add_AC_AdminPage/li_Select application from the'))

WebUI.delay(3)

WebUI.click(findTestObject('Add_AC_AdminPage/div_Attendance Calendar'))

WebUI.delay(2)

'Select an application'
WebUI.click(findTestObject('Add_AC_AdminPage/li_Please Select'))

WebUI.delay(2)

'Check Originating Source'
WebUI.click(findTestObject('Add_AC_AdminPage/i_check_box_outline_blank'))

WebUI.delay(2)

not_run: WebUI.click(findTestObject('Add_AC_AdminPage/li_Please Select'))

WebUI.delay(2)

not_run: WebUI.click(findTestObject('Add_AC_AdminPage/div_Cancel_Save'))

'Click save'
WebUI.click(findTestObject('Add_AC_AdminPage/button_Save'))

WebUI.delay(20)

