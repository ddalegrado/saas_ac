import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

Robot rb = new Robot()

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.setText(findTestObject('Object Repository/Admin Permission/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'lou.gill000+CRTESTAC1ad@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Admin Permission/Page_HRdirect/input_PASSWORD_Password'), 'vTJxSJ71VSN3xk74LjKEvw==')

WebUI.click(findTestObject('AC-TodaysEvent/Page_HRdirect/button_exit_to_app Sign In to'))

WebUI.delay(25)

WebUI.doubleClick(findTestObject('Object Repository/Admin Permission/Page_HRdirect/a'))

WebUI.delay(45)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/div_All Active Employees'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/div_Minyoung Matmak'))

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/button_Add Event'))

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/Admin Permission/Page_HRdirect/select_Select a code'), '1964', 
    true)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Admin Permission/Page_HRdirect/input_Time_timeTaken'), '4')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/button_Save'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/span_18'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/p_Left Early'))

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('Object Repository/Admin Permission/Page_HRdirect/select_Select a code'), '1972', 
    true)

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Admin Permission/Page_HRdirect/input_Time_timeTaken'), '24')

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/button_Save'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/i_cancel'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/i_more_vert'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/a_Download'))

WebUI.delay(10)

rb.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

rb.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.switchToDefaultContent()

WebUI.delay(25)

WebUI.click(findTestObject('Admin Permission/Page_HRdirect/span_18'))

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/div_Minyoung Matmak (1)'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/span_Delete Event'))

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/button_Yes'))

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/span_Kim'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/i_arrow_drop_down'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Admin Permission/Page_HRdirect/div_September'))

