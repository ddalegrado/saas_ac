import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

'Launching SAAS site'
WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

'Keyin registered email address in Email field'
WebUI.setText(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'lou.gill000+CRTESTSMK7@gmail.com')

'Keyin valid password in Passoword field'
WebUI.setEncryptedText(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/input_PASSWORD_Password'), 'vTJxSJ71VSN3xk74LjKEvw==')

'Clicking Signin to my account button'
WebUI.click(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/button_exit_to_app Sign In to'))

WebUI.maximizeWindow()

'Clicking Attendance Calendar application'
WebUI.click(findTestObject('AC_Elements/AttendanceCalendar'))

WebUI.delay(10)

'All Employee dropdown'
WebUI.click(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/div_All Active Employees'))

WebUI.delay(3)

'Bradley Black is selected'
WebUI.click(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/div_Black Bradley'))

WebUI.delay(7)

'Click Add Event button'
WebUI.click(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/button_Add Event'))

WebUI.delay(3)

'Click Absence Code dropdown'
WebUI.click(findTestObject('AddEditDelete/Page_HRdirect/AbsenceCode_dropdown'))

WebUI.delay(3)

'Selecting Injury On Jod from the Absence Code list'
WebUI.click(findTestObject('AddEditDelete/Page_HRdirect/AbsenceCode_InjuryOnJob'))

WebUI.delay(3)

'Selecting Sick from the Time Bank dropdown'
WebUI.selectOptionByValue(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/select_VacationSickNone'), 'D673935', 
    true)

WebUI.delay(3)

'Setting date'
WebUI.setText(findTestObject('AddEditDelete/Page_HRdirect/DatePicker'), '02/11/2020')

WebUI.delay(3)

'Keyin Time field to 12'
WebUI.setText(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/input_Time_timeTaken'), '12')

WebUI.delay(3)

'Saving Absence Event'
WebUI.click(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/button_Save'))

WebUI.delay(3)

'Click a date with event'
WebUI.click(findTestObject('AddEditDelete/Page_HRdirect/Calendar_Feb11'))

WebUI.delay(3)

'Click an event from the Event list'
WebUI.click(findTestObject('AddEditDelete/Page_HRdirect/EventList_selected_event'))

WebUI.delay(3)

'Click Absence Code dropdown'
WebUI.click(findTestObject('AddEditDelete/Page_HRdirect/AbsenceCode_dropdown'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('AddEditDelete/Page_HRdirect/AbsenceCode_Vacation'), 3)

'Selecting Vacation from the Absence Code list'
WebUI.click(findTestObject('AddEditDelete/Page_HRdirect/AbsenceCode_Vacation'))

WebUI.delay(3)

'Selecting Vacation from the Time Bank dropdown'
WebUI.selectOptionByValue(findTestObject('AddEditDelete/Page_HRdirect/select_VacationSickNone'), 'D531520', true)

'Resetting date ot other date'
WebUI.setText(findTestObject('AddEditDelete/Page_HRdirect/DatePicker'), '03/20/2020')

WebUI.delay(3)

'Keyin Time field to 24'
WebUI.setText(findTestObject('AddEditDelete/Page_HRdirect/input_Time_timeTaken'), '24')

WebUI.delay(3)

'Saving the edited absence event'
WebUI.click(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/button_Save'))

WebUI.delay(3)

'Clicking the date with scheduled event'
WebUI.click(findTestObject('AddEditDelete/Page_HRdirect/DatePicker_day19'))

WebUI.delay(3)

'Click an event from the Event list'
WebUI.click(findTestObject('AddEditDelete/Page_HRdirect/EventList_selected_event'))

WebUI.delay(3)

'Clicking the Delete link'
WebUI.click(findTestObject('AddEditDelete/Page_HRdirect/Event_Delete_button'))

WebUI.delay(3)

'Selecting YES from the delete confirmation model'
WebUI.click(findTestObject('AddEditDelete/Page_HRdirect/Event_Delete_modal_YES'))

WebUI.delay(3)

'Checking Profile icon'
WebUI.verifyElementVisible(findTestObject('AddEditDelete/Page_HRdirect/span_Bradley'), FailureHandling.STOP_ON_FAILURE)

'Clicking Profile icon'
WebUI.click(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/span_Bradley'))

WebUI.delay(3)

'Clicking sign out button'
WebUI.click(findTestObject('Object Repository/AddEditDelete/Page_HRdirect/span_Sign Out'))

WebUI.delay(5)

'Verify logout is successful by checking email field in the login page'
WebUI.verifyElementVisible(findTestObject('AddEditDelete/Page_HRdirect/input_EMAIL ADDRESS_Email'))

'Closing browser'
WebUI.closeBrowser()

