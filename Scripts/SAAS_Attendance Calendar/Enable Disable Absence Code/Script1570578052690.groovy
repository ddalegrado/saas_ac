import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

'Launching SAAS site'
WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

'Keyin registered email address in Email field'
WebUI.setText(findTestObject('Enable-Disable Absence Code/Login_EmailField'), 'testnltd2019+4owner@gmail.com')

'Keyin valid password in Passoword field'
WebUI.setEncryptedText(findTestObject('Enable-Disable Absence Code/Login_PasswordField'), 'MHSUC33hkPnBWRnjFBNCPA==')

'Clicking Signin to my account button'
WebUI.click(findTestObject('Enable-Disable Absence Code/Login_SignInButton'))

WebUI.delay(3)

//DISABLE ABSENCE CODE**************************************************
'Launching Attendance Calendar application'
WebUI.click(findTestObject('Enable-Disable Absence Code/AttendanceCalendar_app'))

WebUI.delay(20)

'Clicking Setting tab'
WebUI.click(findTestObject('Enable-Disable Absence Code/AC-Setting_tab'))

WebUI.delay(3)

'Disabling Additional Hour from the Absence Code'
WebUI.click(findTestObject('Enable-Disable Absence Code/Setting_AbsenceCode_AdditionalHour'))

WebUI.delay(3)

'Clicking Calendar tab'
WebUI.click(findTestObject('Enable-Disable Absence Code/AC-Calendar_tab'))

WebUI.delay(3)

'Clicking All Employee dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/AllActiveEmployee_dropdown'))

WebUI.delay(2)

'Selecting Cherish Admin'
WebUI.click(findTestObject('Enable-Disable Absence Code/Employee_Admin,Cherish'))

'Clicking Add Event button'
WebUI.click(findTestObject('Enable-Disable Absence Code/AC-AddEvent_button'))

WebUI.delay(3)

'Clicking Absence Code dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/AbsenceCode_dropdown'))

WebUI.delay(3)

'Checking if Additional Hour absence code is hidden'
WebUI.verifyTextNotPresent('Additional Hours', true)

WebUI.delay(3)

'Clicking Close (x) button'
WebUI.click(findTestObject('Enable-Disable Absence Code/AddAbsenceEvent_Close_button'))

WebUI.delay(2)

'Clicking dashboard icon'
WebUI.click(findTestObject('Enable-Disable Absence Code/Dashboard_icon_AC'))

WebUI.delay(3)

'Launching Time Off Request application'
WebUI.click(findTestObject('Enable-Disable Absence Code/TimeOffRequest_App'))

WebUI.delay(20)

'Clicking Request Time Off dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/RequestTimeOff_dropdown'))

WebUI.delay(2)

'Selecting Freddie Slade list'
WebUI.click(findTestObject('Enable-Disable Absence Code/RequestTimeOff_Owner'))

WebUI.delay(15)

'Scrolling to Absence Code dropdown'
WebUI.scrollToElement(findTestObject('Enable-Disable Absence Code/TOR_AbsenceCode_dropdown'), 2)

WebUI.delay(2)

'Clicking Absence Code dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/TOR_AbsenceCode_dropdown'))

WebUI.delay(2)

'Checking Additional Hours is hidden'
WebUI.verifyTextNotPresent('Additional Hours', true)

WebUI.delay(2)

'Clicking dashboard icon'
WebUI.click(findTestObject('Enable-Disable Absence Code/Dashboard_icon_TOR'))

WebUI.delay(3)

//ENABLE ABSENCE CODE**************************************************
'Launching Attendance Calendar application'
WebUI.click(findTestObject('Enable-Disable Absence Code/AttendanceCalendar_app'))

WebUI.delay(15)

'Clicking Setting tab'
WebUI.click(findTestObject('Enable-Disable Absence Code/AC-Setting_tab'))

WebUI.delay(3)

'Enabling Additional Hour from the Absence Code'
WebUI.click(findTestObject('Enable-Disable Absence Code/Setting_AbsenceCode_AdditionalHour'))

WebUI.delay(3)

'Clicking Calendar tab'
WebUI.click(findTestObject('Enable-Disable Absence Code/AC-Calendar_tab'))

WebUI.delay(3)

'Clicking All Employee dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/AllActiveEmployee_dropdown'))

WebUI.delay(2)

'Selecting Cherish Admin'
WebUI.click(findTestObject('Enable-Disable Absence Code/Employee_Admin,Cherish'))

'Clicking Add Event button'
WebUI.click(findTestObject('Enable-Disable Absence Code/AC-AddEvent_button'))

WebUI.delay(3)

'Clicking Absence Code dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/AbsenceCode_dropdown'))

WebUI.delay(3)

'Checking if Additional Hour absence code is present'
WebUI.verifyTextPresent('Additional Hours', true)

WebUI.delay(3)

'Closing Add Event form'
WebUI.click(findTestObject('Enable-Disable Absence Code/AddAbsenceEvent_Close_button'))

WebUI.delay(2)

'Clicking dashboard icon'
WebUI.click(findTestObject('Enable-Disable Absence Code/Dashboard_icon_AC'))

WebUI.delay(3)

'Launching Timoe Of Request application'
WebUI.click(findTestObject('Enable-Disable Absence Code/TimeOffRequest_App'))

WebUI.delay(15)

'Clicking Request Time Off dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/RequestTimeOff_dropdown'))

WebUI.delay(2)

'Selecting Freddie Slade list'
WebUI.click(findTestObject('Enable-Disable Absence Code/RequestTimeOff_Owner'))

WebUI.delay(15)

'Scrolling to Absence Code dropdown'
WebUI.scrollToElement(findTestObject('Enable-Disable Absence Code/TOR_AbsenceCode_dropdown'), 2)

WebUI.delay(2)

'Clicking Absence Code dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/TOR_AbsenceCode_dropdown'))

WebUI.delay(2)

'Checking if Additional Hour absence code is present'
WebUI.verifyTextPresent('Additional Hours', true)

'Clicking Setting tab'
WebUI.click(findTestObject('Enable-Disable Absence Code/RequestTimeOff_Settings'))

WebUI.delay(2)

'Disabling Additional Hour from the Absence Code'
WebUI.click(findTestObject('Enable-Disable Absence Code/TOR_AbcenseCode_checkbox'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Saving the Absence Code setup'
WebUI.click(findTestObject('Enable-Disable Absence Code/TOR_AbsenceCode_Save_button'))

'Clicking Time Of Request tab'
WebUI.click(findTestObject('Enable-Disable Absence Code/TOR_Tab'))

WebUI.delay(2)

'Clicking Request Time Off dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/RequestTimeOff_dropdown'))

WebUI.delay(2)

'Selecting Freddie Slade list'
WebUI.click(findTestObject('Enable-Disable Absence Code/RequestTimeOff_Owner'))

WebUI.delay(15)

'Scrolling to Absence Code dropdown'
WebUI.scrollToElement(findTestObject('Enable-Disable Absence Code/TOR_AbsenceCode_dropdown'), 2)

WebUI.delay(2)

'Clicking Absence Code dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/TOR_AbsenceCode_dropdown'))

WebUI.delay(2)

'Checking if Additional Hour absence code is hidden'
WebUI.verifyTextNotPresent('Additional Hours', true)

WebUI.delay(0)

'Clicking dashboard icon'
WebUI.click(findTestObject('Enable-Disable Absence Code/Dashboard_icon_TOR'))

WebUI.delay(3)

'Launching Attendance Calendar application'
WebUI.click(findTestObject('Enable-Disable Absence Code/AttendanceCalendar_app'))

WebUI.delay(20)

'Clicking All Employee dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/AllActiveEmployee_dropdown'))

WebUI.delay(2)

'Selecting Cherish Admin'
WebUI.click(findTestObject('Enable-Disable Absence Code/Employee_Admin,Cherish'))

'Clicking Add Event button'
WebUI.click(findTestObject('Enable-Disable Absence Code/AC-AddEvent_button'))

WebUI.delay(3)

'Clicking Absence Code dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/AbsenceCode_dropdown'))

WebUI.delay(3)

'Checking if Additional Hour absence code is hidden'
WebUI.verifyTextNotPresent('Additional Hours', true)

WebUI.delay(2)

'Closing Absence Event form'
WebUI.click(findTestObject('Enable-Disable Absence Code/AddAbsenceEvent_Close_button'))

'Clicking dashboard icon'
WebUI.click(findTestObject('Enable-Disable Absence Code/Dashboard_icon_AC'))

WebUI.delay(2)

'Launching Time Off Request application'
WebUI.click(findTestObject('Enable-Disable Absence Code/TimeOffRequest_App'))

WebUI.delay(3)

'Clicking Settings tab'
WebUI.click(findTestObject('Enable-Disable Absence Code/RequestTimeOff_Settings'))

WebUI.delay(2)

'Enabling Additional Hour from the Absence Code'
WebUI.click(findTestObject('Enable-Disable Absence Code/TOR_AbcenseCode_checkbox'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Saving Absence Code settings'
WebUI.click(findTestObject('Enable-Disable Absence Code/TOR_AbsenceCode_Save_button'))

'Clicking Time Of Request tab'
WebUI.click(findTestObject('Enable-Disable Absence Code/TOR_Tab'))

WebUI.delay(2)

'Clicking Request Time Off dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/RequestTimeOff_dropdown'))

WebUI.delay(2)

'Selecting Freddie Slade list'
WebUI.click(findTestObject('Enable-Disable Absence Code/RequestTimeOff_Owner'))

WebUI.delay(15)

'Scrolling to Absence Code dropdown'
WebUI.scrollToElement(findTestObject('Enable-Disable Absence Code/TOR_AbsenceCode_dropdown'), 2)

WebUI.delay(2)

'Clicking Absence Code dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/TOR_AbsenceCode_dropdown'))

WebUI.delay(2)

'Checking if Additional Hour absence code is present'
WebUI.verifyTextPresent('Additional Hours', true)

WebUI.delay(2)

'Clicking dashboard icon'
WebUI.click(findTestObject('Enable-Disable Absence Code/Dashboard_icon_TOR'))

WebUI.delay(2)

'Launching Attendance Calendar application'
WebUI.click(findTestObject('Enable-Disable Absence Code/AttendanceCalendar_app'))

WebUI.delay(20)

'Clicking All Employee dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/AllActiveEmployee_dropdown'))

WebUI.delay(2)

'Selecting Cherish Admin'
WebUI.click(findTestObject('Enable-Disable Absence Code/Employee_Admin,Cherish'))

'Clicking Add Event button'
WebUI.click(findTestObject('Enable-Disable Absence Code/AC-AddEvent_button'))

WebUI.delay(3)

'Clicking Absence Code dropdown'
WebUI.click(findTestObject('Enable-Disable Absence Code/AbsenceCode_dropdown'))

WebUI.delay(3)

'Checking if Additional Hour absence code is present'
WebUI.verifyTextPresent('Additional Hours', true)

WebUI.delay(2)

'Closing Absence Event form'
WebUI.click(findTestObject('Enable-Disable Absence Code/AddAbsenceEvent_Close_button'))

'Clicking dashboard icon'
WebUI.click(findTestObject('Enable-Disable Absence Code/Dashboard_icon_AC'))

WebUI.delay(3)

'Clicking Profile icon'
WebUI.click(findTestObject('Enable-Disable Absence Code/Profile_dropdown'))

WebUI.delay(2)

'Clicking Sign out button'
WebUI.click(findTestObject('Enable-Disable Absence Code/SignOut_button'))

WebUI.delay(5)

'Verifying if log out is successful by checking email field element'
WebUI.verifyElementVisible(findTestObject('Enable-Disable Absence Code/Login_EmailField'))

WebUI.delay(2)

'Closing browser'
WebUI.closeBrowser()

