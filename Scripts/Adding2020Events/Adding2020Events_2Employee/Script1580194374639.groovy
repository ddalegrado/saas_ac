import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://account.hrdirectappsqa.com/')

WebUI.delay(10)

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

'login user name'
WebUI.click(findTestObject('Employee Status Filter/input_EMAIL ADDRESS_Email'), FailureHandling.STOP_ON_FAILURE)

'login user name'
WebUI.setText(findTestObject('Employee Status Filter/input_EMAIL ADDRESS_Email'), 'tstestp1+38QA@gmail.com')

'login password'
WebUI.click(findTestObject('Employee Status Filter/input_PASSWORD_Password'), FailureHandling.STOP_ON_FAILURE)

'login password'
WebUI.setText(findTestObject('Employee Status Filter/input_PASSWORD_Password'), 'Password_3')

'login button'
WebUI.click(findTestObject('Employee Status Filter/button_exit_to_app Sign In to My Account'))

WebUI.delay(5)

'click AC from dashboard'
WebUI.click(findTestObject('Employee Status Filter/Launch_AC'))

WebUI.delay(15)

'clicking Employee dropdown'
WebUI.click(findTestObject('Employee Status Filter/div_All Active Employees'))

WebUI.delay(3)

'selecting employee TWO from the dropdown'
WebUI.click(findTestObject('Employee Status Filter/employee_BeautyTwo'))

WebUI.delay(3)

'calling script for adding events in Jan 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_Jan2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'calling script for adding events in Feb 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_Feb2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'calling script for adding events in Mar 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_Mar2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'calling script for adding events in Apr 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_Apr2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'calling script for adding events in May 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_May2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'calling script for adding events in Jun 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_Jun2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'calling script for adding events in Jul 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_Jul2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'calling script for adding events in Aug 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_Aug2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'calling script for adding events in Sept 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_Sept2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'calling script for adding events in Oct 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_Oct2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'calling script for adding events in Nov 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_Nov2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'calling script for adding events in Dec 2020'
WebUI.callTestCase(findTestCase('Adding2020Events/AddingEvents_Dec2020'), [:], FailureHandling.STOP_ON_FAILURE)

'year / month view dropdown'
WebUI.click(findTestObject('Employee Status Filter/button_year'))

'select YEARLY view'
WebUI.click(findTestObject('Employee Status Filter/a_Year'))

'profile dropdown'
WebUI.click(findTestObject('Add2019Events/i_arrow_drop_down'))

'user logout'
WebUI.click(findTestObject('Add2019Events/span_Sign Out'))

