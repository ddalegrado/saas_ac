<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_gradient</name>
   <tag></tag>
   <elementGuidId>13072bec-ee91-4305-9664-bb4e8a42019b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//i[@class = 'material-icons icon-gradient-ltblue']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-icons icon-gradient-ltblue</value>
   </webElementProperties>
</WebElementEntity>
