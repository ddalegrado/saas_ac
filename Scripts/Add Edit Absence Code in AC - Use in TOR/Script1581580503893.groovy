import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

'Execute Create Account test case'
WebUI.callTestCase(findTestCase('Create New Account'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

'Select AC app from the Dashboard'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/Purchase_AC'))

'clic Get Started button'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Get Started'))

'Input Address Line1'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input__address1'), '123 Main Dove Street')

'Input City'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input__city'), 'Tampa')

'Scroll and select State'
WebUI.selectOptionByValue(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/select_State'), 'FL', true)

'Input Zip code'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input__postal_code'), '33013')

'Input Credit Card info'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input_CC number'), '4012 8888 8888 1881')

WebUI.delay(2)

'Input expiry month'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input_Month'), '12')

WebUI.delay(3)

'Input expiry year'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input_Year'), '2023')

WebUI.delay(2)

'Input cvv'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input_CVV'), '123')

WebUI.delay(2)

'click Pay now'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Pay Now'))

WebUI.delay(75)

WebUI.delay(2)

'click NEXT'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Next'))

WebUI.delay(2)

WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Next_2'))

WebUI.delay(2)

WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/span_Yes'))

WebUI.delay(2)

WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Next_2'))

WebUI.delay(2)

WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Complete'))

WebUI.delay(20)

WebUI.delay(2)

WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/AC_Settings'))

WebUI.delay(2)

'Click Create New Code'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/span_Create New Code'))

'Enter custom absensce code'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input_new_AbsenceCode'), 'AA')

WebUI.delay(2)

'Enter code description'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input_Description_new_AbsenceCode'), 
    'Custom Code - AC')

WebUI.delay(2)

'Select code color'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/span_Select Code Color'))

WebUI.delay(2)

'Save new custom code'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Save'))

WebUI.delay(3)

'Get value entered as new code = Set in Line 38'
New_Code = WebUI.getText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/NewCode_AC'))

WebUI.delay(2)

'Go to the dashboard'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/img_SmartApps'))

'Click TOR'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/select_TOR'))

WebUI.delay(2)

WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Get Started_TOR'))

'Purchase TOR app'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Pay Now'))

WebUI.delay(15)

'Get Started on Time Off Request'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Get Started_TOR_First Launch'))

WebUI.delay(2)

'Go to Settings tab'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/TOR_Settings'))

WebUI.delay(2)

'Click on Custom Code added in AC app'
AddedCode = WebUI.getText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/Added Code'))

'Compare code added in AC with the code shown in TOR app'
WebUI.verifyMatch(New_Code, AddedCode, false)

WebUI.delay(2)

'Go to time off request tab'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/a_Time Off Requests'))

WebUI.delay(2)

'click time off request'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/span_Request Time Off'))

WebUI.delay(2)

'select employee = Owner'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/div_Owner Owner'))

WebUI.delay(2)

'enter TOR reason'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input__request reason'), 'custom code AC Test')

WebUI.delay(2)

'click paid'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/span_Paid'))

WebUI.delay(2)

'select date'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/span_5'))

WebUI.delay(2)

'click on Scroll bar'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/Scroll_absenceCode'))

WebUI.delay(2)

'Select an absence code'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/Select_Absence Code'))

WebUI.delay(2)

'Click Approve button'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Approve'))

'Confirm approve'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Approve_1'))

WebUI.delay(2)

'Go back to the dashboard'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/img'))

WebUI.delay(2)

'Launch AC app'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/a_Calendar'))

WebUI.delay(25)

'Go to Settings tab'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/AC_Settings'))

WebUI.delay(2)

'Click option'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/i_more_vert'))

WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/a_Edit'))

WebUI.delay(2)

'Edit Custom Code Description'
WebUI.setText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/input_Description_Edit'), 'Custom Code edit - AC')

WebUI.delay(2)

'Save'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/button_Save'))

WebUI.delay(2)

'Get value code description'
AC_EditCodeDesc = WebUI.getText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/Custom Code Edited'))

WebUI.delay(2)

'Go to Dashboard'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/img_SmartApps'))

WebUI.delay(2)

'Select TOR'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/a_TOR'))

WebUI.delay(10)

'-----TO BE REMOVED ONCE ISSUE IS FIXED----------'
WebUI.click(findTestObject('Add Edit Absence Code in TOR - Use in AC/Page_HRdirect/button_Get Started'))

WebUI.delay(2)

'Click Settings tab'
WebUI.click(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/TOR_Settings'))

WebUI.delay(2)

'Check the description of custom code in TOR'
TOR_EditedCodeDesc = WebUI.getText(findTestObject('Add Edit Absence Code in AC - Use in TOR/Page_HRdirect/TOR_EditCodeDesc'))

'Verify that updated code in AC is shown in TOR'
WebUI.verifyMatch(AC_EditCodeDesc, TOR_EditedCodeDesc, false)

