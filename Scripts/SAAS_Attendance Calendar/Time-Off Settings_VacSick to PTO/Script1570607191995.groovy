import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://account.hrdirectappsqa.com/')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'donatest1+SAAS384@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/input_PASSWORD_Password'), 
    '3Z9vEXbPUsB23Jtp1+ckNg==')

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_exit_to_app Sign In to My Account'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/a'))

WebUI.delay(10)

not_run: WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_Attendance-Calendar-IVpdf - HRdirect/a_Try Beta'))

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/div_All Active Employees'))

not_run: WebUI.delay(3)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/div_FF                                                                                                        Four Four (1)'))

WebUI.delay(3)

not_run: WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_year'))

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_No events today_calendar-toggle__switch'))

WebUI.delay(5)

WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (1)'), '100')

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (1)'))

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save'))

WebUI.delay(10)

WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (2)'), '100')

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (2)'))

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save'))

WebUI.delay(10)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/button_Add Event'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/div_Select a code'))

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Leave of Absence'))

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/select_Select a code'), 
    '1179', true)

WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input__datePickerComponent'), '02/06/2020')

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Time_timeTaken'))

WebUI.setText(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/input_Time_timeTaken'), '10')

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save_1'))

WebUI.delay(10)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/button_Add Event'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/div_Select a code'))

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Illness-Self (1)'))

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/select_VacationSickNone'), 
    'D673935', true)

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/select_Select a code'), 
    '1176', true)

not_run: WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input__datePickerComponent'), '10/04/2019')

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/select_VacationSickNone'))

WebUI.selectOptionByLabel(findTestObject('Settings_VacSickToPTO/Page_HRdirect/select_VacationSickNone'), 'Sick', true)

WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input__datePickerComponent'), '02/04/2020')

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Time_timeTaken'))

WebUI.setText(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/input_Time_timeTaken'), '5')

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save_2'))

WebUI.delay(10)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/a_Settings (1)'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/span_PTO'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save_3'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/a_Calendar (1)'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/div_All Active Employees'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/div_FF                                                                                                        Four Four (1)'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_No events today_calendar-toggle__switch'))

not_run: WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/a_Month'))

WebUI.delay(10)

WebUI.verifyElementPresent(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Leave of Absence (3)'), 5)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Leave of Absence (3)'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_four (1)'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/span_Delete Event'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Yes'))

WebUI.delay(10)

WebUI.verifyElementPresent(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Illness-Self (4)'), 3)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_Illness-Self (4)'))

WebUI.delay(3)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/span_four (1)'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/span_Delete Event'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Yes'))

WebUI.delay(10)

WebUI.setText(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (1)'), '0')

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/input_Starting Hours_vacationStartTime (1)'))

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save'))

WebUI.delay(10)

WebUI.click(findTestObject('Settings_VacSickToPTO/Page_HRdirect/a_Settings (1)'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/span_Vacation  Sick'))

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/button_Save_4'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/i_arrow_drop_down'))

WebUI.click(findTestObject('Object Repository/Settings_VacSickToPTO/Page_HRdirect/span_Sign Out'))

WebUI.closeBrowser()

