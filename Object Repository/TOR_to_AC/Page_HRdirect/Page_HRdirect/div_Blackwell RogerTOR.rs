<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Blackwell RogerTOR</name>
   <tag></tag>
   <elementGuidId>b6662c10-5735-40a7-b386-866618ffa46d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='2329']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;2329&quot;)[count(. | //div[@class = 'grid-row' and @id = '2329' and (text() = '
                    
                        
                            
                                
                                
                            
                            
                                Blackwell, Roger
                            
                        
                    
                    
                         02/04/2019 - 02/06/2019 
                        

                    
                    
                        
                        
                            
                            
                            check_circle
                            
                            
                            Approved
                        
                        M - Military Leave

                    
                    
                        
                        
                            10/15/2019
                        
                    
                ' or . = '
                    
                        
                            
                                
                                
                            
                            
                                Blackwell, Roger
                            
                        
                    
                    
                         02/04/2019 - 02/06/2019 
                        

                    
                    
                        
                        
                            
                            
                            check_circle
                            
                            
                            Approved
                        
                        M - Military Leave

                    
                    
                        
                        
                            10/15/2019
                        
                    
                ')]) = count(//div[@class = 'grid-row' and @id = '2329' and (text() = '
                    
                        
                            
                                
                                
                            
                            
                                Blackwell, Roger
                            
                        
                    
                    
                         02/04/2019 - 02/06/2019 
                        

                    
                    
                        
                        
                            
                            
                            check_circle
                            
                            
                            Approved
                        
                        M - Military Leave

                    
                    
                        
                        
                            10/15/2019
                        
                    
                ' or . = '
                    
                        
                            
                                
                                
                            
                            
                                Blackwell, Roger
                            
                        
                    
                    
                         02/04/2019 - 02/06/2019 
                        

                    
                    
                        
                        
                            
                            
                            check_circle
                            
                            
                            Approved
                        
                        M - Military Leave

                    
                    
                        
                        
                            10/15/2019
                        
                    
                ')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>grid-row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>2329</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                            
                                
                                
                            
                            
                                Blackwell, Roger
                            
                        
                    
                    
                         02/04/2019 - 02/06/2019 
                        

                    
                    
                        
                        
                            
                            
                            check_circle
                            
                            
                            Approved
                        
                        M - Military Leave

                    
                    
                        
                        
                            10/15/2019
                        
                    
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;2329&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <value>//div[@id='2329']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//timeoff-requests-main[@id='wrapper']/div/requests-list/div/div[2]/div[2]/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submitted On'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//requests-list/div/div[2]/div[2]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
