import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Add2019Events/Page_HRdirect/span_Sept1'))

WebUI.selectOptionByLabel(findTestObject('Add2019Events/Page_HRdirect/select_Select a code                       _e3a280'), 
    'L-Leave of Absence', true)

WebUI.setText(findTestObject('Add2019Events/Page_HRdirect/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Add2019Events/Page_HRdirect/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Add2019Events/Page_HRdirect/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Add2019Events/Page_HRdirect/span_Sept2'))

WebUI.selectOptionByLabel(findTestObject('Add2019Events/Page_HRdirect/select_Select a code                       _e3a280'), 
    'LE-Left Early', true)

WebUI.selectOptionByLabel(findTestObject('Add2019Events/Page_HRdirect/select_VacationSickNone'), 'Sick', true)

WebUI.setText(findTestObject('Add2019Events/Page_HRdirect/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Add2019Events/Page_HRdirect/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Add2019Events/Page_HRdirect/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Add2019Events/Page_HRdirect/span_Sept3'))

WebUI.selectOptionByLabel(findTestObject('Add2019Events/Page_HRdirect/select_Select a code                       _e3a280'), 
    'LO-Layoff', true)

WebUI.setText(findTestObject('Add2019Events/Page_HRdirect/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Add2019Events/Page_HRdirect/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Add2019Events/Page_HRdirect/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Add2019Events/Page_HRdirect/span_Sept4'))

WebUI.selectOptionByLabel(findTestObject('Add2019Events/Page_HRdirect/select_Select a code                       _e3a280'), 
    'M-Military Leave', true)

WebUI.selectOptionByLabel(findTestObject('Add2019Events/Page_HRdirect/select_VacationSickNone'), 'Sick', true)

WebUI.setText(findTestObject('Add2019Events/Page_HRdirect/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Add2019Events/Page_HRdirect/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Add2019Events/Page_HRdirect/button_Save'))

WebUI.delay(5)

