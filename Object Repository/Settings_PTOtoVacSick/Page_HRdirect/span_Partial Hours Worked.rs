<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Partial Hours Worked</name>
   <tag></tag>
   <elementGuidId>b27d6c5c-2b82-4128-8200-2a040cd922b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//platform-apps-main[@id='wrapper']/div/ng-component/div/div[2]/calendar-component/div[3]/calendar-month-component/div[2]/div/div/div[3]/div/div[5]/div[2]/div/div/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Partial Hours Worked' or . = 'Partial Hours Worked')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>truncate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Partial Hours Worked</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wrapper&quot;)/div[@class=&quot;content-main-scroller&quot;]/ng-component[1]/div[@class=&quot;main&quot;]/div[@class=&quot;content&quot;]/calendar-component[1]/div[@class=&quot;calendar&quot;]/calendar-month-component[1]/div[@class=&quot;month-view&quot;]/div[@class=&quot;month-box&quot;]/div[@class=&quot;month&quot;]/div[@class=&quot;monthdays&quot;]/div[@class=&quot;week&quot;]/div[@class=&quot;day hasEvent&quot;]/div[@class=&quot;event-area&quot;]/div[@class=&quot;event-chip&quot;]/div[@class=&quot;event-name&quot;]/span[@class=&quot;truncate&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//platform-apps-main[@id='wrapper']/div/ng-component/div/div[2]/calendar-component/div[3]/calendar-month-component/div[2]/div/div/div[3]/div/div[5]/div[2]/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='add'])[11]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='add'])[10]/following::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delgado, B.'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='add'])[12]/preceding::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[5]/div[2]/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
