import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Adding2020Events/span_September'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Excused'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/02/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_FMLA'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/03/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Injury on Job'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/04/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Excused'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/05/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_FMLA'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/06/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Injury on Job'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/07/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Excused'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/08/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_FMLA'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/09/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Injury on Job'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/10/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Excused'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/11/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_FMLA'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/12/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Injury on Job'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/13/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Excused'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/14/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_FMLA'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/15/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Injury on Job'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/16/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Excused'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/17/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_FMLA'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/18/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Injury on Job'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/19/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Excused'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/20/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_FMLA'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/21/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Injury on Job'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/22/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Excused'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/23/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_FMLA'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/24/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

WebUI.click(findTestObject('Adding2020Events/span_Injury on Job'))

WebUI.delay(3)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '09/25/2020')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

