import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.sendKeys(findTestObject('Login/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'testnltd2019+4owner@gmail.com')

WebUI.setEncryptedText(findTestObject('Login/Page_HRdirect/input_PASSWORD_Password'), 'MHSUC33hkPnBWRnjFBNCPA==')

WebUI.click(findTestObject('Login/Page_HRdirect/button_exit_to_app Sign In to'))

WebUI.maximizeWindow()

WebUI.delay(10)

WebUI.verifyElementVisible(findTestObject('AC_Elements/AttendanceCalendar_App'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('AC_Elements/AttendanceCalendar_App'))

WebUI.delay(15)

WebUI.click(findTestObject('AC_Elements/AllActiveEmployees_Dropdown'))

WebUI.delay(5)

WebUI.click(findTestObject('AC_Elements/AdminCherish_Employee'))

WebUI.delay(2)

WebUI.click(findTestObject('AC_Elements/AddEvent_Button'))

WebUI.delay(2)

not_run: WebUI.click(findTestObject('AC_Elements/Added Event/Selected_Mar4-2019'))

WebUI.click(findTestObject('AC_Elements/Add Absence Event/AbsenceCode_Dropdown'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('AC_Elements/Add Absence Event/AbsenceCode_Vacation'), 2)

WebUI.click(findTestObject('AC_Elements/Add Absence Event/AbsenceCode_Vacation'))

WebUI.delay(2)

WebUI.setText(findTestObject('AC_Elements/Add Absence Event/Date_Picker'), '03/05/2019')

WebUI.setText(findTestObject('AC_Elements/Add Absence Event/Notes_Textarea'), 'Automation testing on-going!!!!')

WebUI.delay(2)

WebUI.click(findTestObject('AC_Elements/Add Absence Event/Save_Button'))

WebUI.delay(5)

WebUI.click(findTestObject('AC_Elements/Year_Arrow_Left'))

WebUI.click(findTestObject('AC_Elements/Added Event/Selected_Mar4-2019'))

WebUI.delay(2)

WebUI.click(findTestObject('AC_Elements/Added Event/Event_added'))

WebUI.delay(2)

WebUI.setText(findTestObject('AC_Elements/Add Absence Event/Date_Picker'), '03/05/2020')

WebUI.click(findTestObject('AC_Elements/Add Absence Event/Notes_Textarea'))

not_run: WebUI.selectOptionByValue(findTestObject('AC_Elements/Add Absence Event/Date_Picker_Year_Dropdown'), '2020', false)

WebUI.delay(3)

not_run: WebUI.click(findTestObject('AC_Elements/Added Event/TransferToNextYear'))

not_run: WebUI.delay(3)

WebUI.click(findTestObject('AC_Elements/Add Absence Event/Save_Button'))

WebUI.delay(5)

not_run: WebUI.click(findTestObject('AC_Elements/Add Absence Event/NextYear_Close_button'))

not_run: WebUI.delay(3)

WebUI.click(findTestObject('AC_Elements/Year_Arrow_Right'))

not_run: WebUI.delay(5)

WebUI.click(findTestObject('AC_Elements/Added Event/Transfer_NexYear'))

not_run: WebUI.delay(5)

WebUI.click(findTestObject('AC_Elements/Added Event/New_TransferredEvent'))

WebUI.delay(2)

not_run: WebUI.click(findTestObject('AC_Elements/Added Event/Selected_Mar4-2020'))

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('AC_Elements/Add Absence Event/EditAbsenceEvent_Close'))

WebUI.delay(3)

WebUI.click(findTestObject('AC_Elements/Add Absence Event/Transferred_event_close_button'))

not_run: WebUI.delay(3)

WebUI.click(findTestObject('AC_Elements/Profile_dropdown'))

WebUI.delay(2)

WebUI.click(findTestObject('AC_Elements/SignOut_button'))

WebUI.delay(5)

WebUI.verifyElementPresent(findTestObject('AC_Elements/Login_Panel'), 3)

WebUI.delay(2)

WebUI.closeBrowser()

