import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

'Navigate to site'
WebUI.navigateToUrl('https://account.hrdirectappsstaging.com/Home/Login/')

WebUI.click(findTestObject('Object Repository/Create New Account/Page_HRdirect/a_SIGN UP'))

WebUI.maximizeWindow()

WebUI.delay(3)

'Get current date and time'
time = new Date().getTime()

//timeformat = time.format('hhmmss')
'add current date and time in email add\r\n'
Email = (('qa' + time) + '_crtest@yopmail.com')

WebUI.setText(findTestObject('Object Repository/Create New Account/Page_HRdirect/input_EMAIL ADDRESS'), Email)

'input password'
WebUI.setEncryptedText(findTestObject('Object Repository/Create New Account/Page_HRdirect/input_PASSWORD'), 'cHBFEg1ykROLjuIVhbxYXA==')

'click \'Create Account\' button'
WebUI.click(findTestObject('Object Repository/Create New Account/Page_HRdirect/button_CREATE AN ACCOUNT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Create New Account/Page_HRdirect/button_Next'))

'Input Company Name'
WebUI.setText(findTestObject('Object Repository/Create New Account/Page_HRdirect/input_CompanyName'), 'AC Company')

'Select Industry'
WebUI.selectOptionByValue(findTestObject('Object Repository/Create New Account/Page_HRdirect/select_Industry'), '2', true)

'Select Company Size'
WebUI.selectOptionByValue(findTestObject('Object Repository/Create New Account/Page_HRdirect/select_CompanySize'), '1', 
    true)

WebUI.delay(3)

'click Next'
WebUI.click(findTestObject('Object Repository/Create New Account/Page_HRdirect/button_Next'))

WebUI.delay(3)

'Input First Name'
WebUI.setText(findTestObject('Object Repository/Create New Account/Page_HRdirect/input_First Name_FirstName'), 'Owner')

'Input Last Name'
WebUI.setText(findTestObject('Object Repository/Create New Account/Page_HRdirect/input_Last Name_LastName'), 'Owner')

'Input Phone number'
WebUI.setText(findTestObject('Object Repository/Create New Account/Page_HRdirect/input_Phone Number_Phone'), '(876) 543-2109')

WebUI.delay(3)

WebUI.doubleClick(findTestObject('Create New Account/Page_HRdirect/Carousel bottom'))

WebUI.delay(3)

'Click Save'
WebUI.click(findTestObject('Object Repository/Create New Account/Page_HRdirect/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Create New Account/Page_HRdirect/img'))

