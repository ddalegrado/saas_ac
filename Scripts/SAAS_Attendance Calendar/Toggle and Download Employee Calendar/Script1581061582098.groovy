import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://account.hrdirectappsqa.com/')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Toggle and Download Employee Calendar/Resources_Download Blank Calendars/input_EMAIL ADDRESS_Email'), 
    'tstestp1+38QA@gmail.com')

WebUI.setEncryptedText(findTestObject('Toggle and Download Employee Calendar/Resources_Download Blank Calendars/input_PASSWORD_Password'), 
    'p4y+y39Ir5MGqsTUM1wOYw==')

WebUI.click(findTestObject('Toggle and Download Employee Calendar/Resources_Download Blank Calendars/button_exit_to_app Sign In to My Account'))

WebUI.delay(5)

WebUI.click(findTestObject('Toggle and Download Employee Calendar/Resources_Download Blank Calendars/a_AttendanceCalendar'))

WebUI.delay(15)

WebUI.delay(5)

'click Employee dropdown'
WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/div_All Active Employees'))

'Select Employee Name: Attitude One'
WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/div_Attitude One'))

WebUI.delay(5)

WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/i_more_vert'))

'Download 2020 Calendar'
WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/a_Download'))

WebUI.delay(10)

WebUI.callTestCase(findTestCase('Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.switchToWindowTitle('HRdirect')

'Switch to 2019 Calendar'
WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/i_keyboard_arrow_right'))

WebUI.delay(2)

'Switch to 2019 Calendar'
WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/i_keyboard_arrow_right'))

WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/i_more_vert'))

WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/a_Download'))

WebUI.delay(10)

'Download 2019 Calendar'
WebUI.callTestCase(findTestCase('Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowTitle('HRdirect')

'Switch to 2018 Calendar'
WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/i_keyboard_arrow_left'))

WebUI.delay(3)

WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/i_more_vert'))

WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/a_Download'))

WebUI.delay(10)

'Download 2018 Calendar'
WebUI.callTestCase(findTestCase('Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowTitle('HRdirect')

'Switch to 2017 Calendar'
WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/i_keyboard_arrow_left'))

WebUI.delay(3)

WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/i_more_vert'))

'Download 2017 Calendar'
WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/a_Download'))

WebUI.delay(10)

WebUI.callTestCase(findTestCase('Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowTitle('HRdirect')

'Switch to 2016 Calendar'
WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/i_keyboard_arrow_left'))

WebUI.delay(3)

WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/i_more_vert'))

'Download 2016 Calendar'
WebUI.click(findTestObject('Toggle and Download Employee Calendar/Download_EmployeeCalendar/a_Download'))

WebUI.delay(10)

WebUI.callTestCase(findTestCase('Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

