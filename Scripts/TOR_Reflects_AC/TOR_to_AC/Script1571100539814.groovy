import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'edqatester+crtestvalidatec14@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/input_PASSWORD_Password'), 'xXYsV5zEcvj6nqyRL3fcug==')

WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/button_exit_to_app Sign In to'))

WebUI.delay(6)

'click TOR app from the dashboard'
WebUI.click(findTestObject('TOR_to_AC/Page_HRdirect/a'))

WebUI.delay(25)

'click time off request dropdown'
WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/i_arrow_drop_down'))

WebUI.delay(2)

'select employee from the droplist'
WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/div_Blackwell Roger'))

WebUI.delay(15)

WebUI.setText(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/input__Q570404'), 'TOR approved')

WebUI.delay(5)

'select paid radio button'
WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/span_Paid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/label_LIST VIEW'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/input__StartDate'), '02/04/2019')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/input__EndDate'), '02/06/2019')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/input_Mon_form-control ng-unto'), '4')

WebUI.delay(2)

not_run: WebUI.setText(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/input_Tue_form-control ng-unto'), '5')

not_run: WebUI.delay(2)

not_run: WebUI.setText(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/input_Wed_form-control ng-unto'), '6')

not_run: WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/input_Comments_Q157341'), 'approved')

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('TOR_to_AC/Page_HRdirect/Page_HRdirect/select_Click_absenceCodeDropdown'), 2)

'click absence code dropdown'
WebUI.click(findTestObject('TOR_to_AC/Page_HRdirect/select_Click_absenceCodeDropdown'))

WebUI.delay(2)

'select military leave'
WebUI.selectOptionByValue(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/select_Click to Select'), '15', true)

WebUI.delay(2)

'click approve button'
WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/button_Approve'))

WebUI.delay(2)

'click approve button form the pop-up'
WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/button_Approve_1'))

WebUI.delay(2)

WebUI.navigateToUrl('https://calendar.hrdirectappsqa.com/events')

'validate the added request shows on classic calendar'
WebUI.delay(30)

'click all active employee dropdown'
WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/div_All Active Employees'))

WebUI.delay(10)

'select employee (use this object)'
WebUI.click(findTestObject('TOR_to_AC/Page_Attendance-Calendar-IV.pdf - H/Page_HRdirect/div_Blackwell Roger_FF'))

WebUI.delay(10)

'validate on feb moth view, request is displayed'
WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/span_February'))

WebUI.delay(2)

WebUI.navigateToUrl('https://timeoff.hrdirectappsqa.com/timeoffrequests')

WebUI.delay(25)

'select time off request'
WebUI.click(findTestObject('TOR_to_AC/Page_HRdirect/Page_HRdirect/div_Blackwell Roger_TOR_Grid'))

WebUI.delay(25)

WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/span_Cancel Request'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/button_Yes'))

WebUI.delay(8)

WebUI.navigateToUrl('https://calendar.hrdirectappsqa.com/events')

WebUI.delay(30)

WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/div_All Active Employees'))

WebUI.delay(2)

WebUI.click(findTestObject('TOR_to_AC/Page_Attendance-Calendar-IV.pdf - H/Page_HRdirect/div_Blackwell Roger_FF_AC'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/span_February'))

WebUI.delay(15)

not_run: WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/i_arrow_drop_down'))

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('Object Repository/TOR_to_AC/Page_HRdirect/span_Sign Out'))

not_run: WebUI.delay(5)

WebUI.closeBrowser()

