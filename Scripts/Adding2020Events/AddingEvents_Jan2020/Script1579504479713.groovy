import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

'select month JANUARY'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_January'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Additional Hours'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Additional Hours'))

WebUI.delay(3)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '01/02/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Partial Hours Worked'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Partial Hours Worked'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '01/03/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Doctors Appointment'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Doctors Appointment'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '01/04/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

