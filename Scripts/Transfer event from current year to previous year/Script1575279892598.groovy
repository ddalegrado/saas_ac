import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/input_EMAIL ADDRESS_Email'), 
    'testnltd2019+4owner@gmail.com')

WebUI.setEncryptedText(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/input_PASSWORD_Password'), 
    'MHSUC33hkPnBWRnjFBNCPA==')

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/button_exit_to_app Sign In to My Account'))

WebUI.delay(10)

'Launch Calendar App'
WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/a_Calendar'))

WebUI.delay(20)

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/div_All Active Employees'))

WebUI.delay(5)

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/Employee_Kaylyn'))

WebUI.delay(2)

'Add Event'
WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/AddEvent_Button'))

WebUI.delay(2)

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/select_Select a code'))

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/AbsenceCode_Vacation'))

WebUI.setText(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/input_Time_timeTaken'), 
    '2')

WebUI.setText(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/Date_Picker'), '01/20/2020')

WebUI.setText(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/textarea_Notes_notes'), 
    'to be transferred to previous year')

WebUI.delay(2)

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/AddedEvent_CurrentYear'))

WebUI.delay(2)

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/Event_CurrentYear'))

WebUI.delay(2)

WebUI.setText(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/Date_Picker'), '01/20/2018')

WebUI.delay(2)

WebUI.setText(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/textarea_Notes_notes'), 
    'Transferred from current year')

WebUI.delay(2)

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/button_Save'))

WebUI.delay(5)

'Arrow left on Current Year - To go to previous year'
WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/Year_Arrow_Left'))

WebUI.delay(2)

'Arrow left on Current Year - To go to previous year'
WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/Year_Arrow_Left'))

WebUI.delay(2)

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/Event_PreviousYear'))

WebUI.delay(2)

WebUI.verifyTextPresent('Kaylynn', true)

WebUI.delay(2)

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/Transferred_event_close_button'))

WebUI.delay(3)

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/Profile_dropdown'))

WebUI.delay(3)

WebUI.click(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/SignOut_button'))

WebUI.delay(4)

WebUI.verifyElementPresent(findTestObject('Transfer event from current year to previous year/Page_HRdirect (1)/Login_Panel'), 
    1)

WebUI.delay(2)

WebUI.closeBrowser()

