<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Emp_AttitudeOne</name>
   <tag></tag>
   <elementGuidId>f30e594d-e68c-4276-9a68-2c4d08aa5594</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div[1]/platform-apps-main/div/ng-component/div/div[1]/employee-list-search-component/div/div[2]/ul/content[1]/li[4]/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[(text() = 'Attitude, One' or . = 'Attitude, One')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div[1]/platform-apps-main/div/ng-component/div/div[1]/employee-list-search-component/div/div[2]/ul/li[5]/div/div[2]/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>name truncate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Attitude, One</value>
   </webElementProperties>
</WebElementEntity>
