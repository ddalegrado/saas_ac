<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Click to Select_TOR</name>
   <tag></tag>
   <elementGuidId>090c8221-c635-4a8b-bb76-d321f13f04d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = concat('
             Click to Select 
            AA - Custom Code - ACA - Additional HoursB - BereavementC - Partial Hours WorkedD - Doctor' , &quot;'&quot; , 's AppointmentE - ExcusedF - FMLAG - Injury on JobH - HolidayI - Illness-SelfJ - Jury DutyK - TerminationL - Leave of AbsenceLE - Left EarlyLO - LayoffM - Military LeaveN - No Cal/No ShowP - PersonalS - SuspensionT - TardyU - UnexcusedV - VacationX - Illness in the FamilyY - Floating HolidayZ - Last Day Worked
        ') or . = concat('
             Click to Select 
            AA - Custom Code - ACA - Additional HoursB - BereavementC - Partial Hours WorkedD - Doctor' , &quot;'&quot; , 's AppointmentE - ExcusedF - FMLAG - Injury on JobH - HolidayI - Illness-SelfJ - Jury DutyK - TerminationL - Leave of AbsenceLE - Left EarlyLO - LayoffM - Military LeaveN - No Cal/No ShowP - PersonalS - SuspensionT - TardyU - UnexcusedV - VacationX - Illness in the FamilyY - Floating HolidayZ - Last Day Worked
        '))]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='Q322585']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-untouched ng-pristine ng-invalid</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Q322585</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
             Click to Select 
            AA - Custom Code - ACA - Additional HoursB - BereavementC - Partial Hours WorkedD - Doctor's AppointmentE - ExcusedF - FMLAG - Injury on JobH - HolidayI - Illness-SelfJ - Jury DutyK - TerminationL - Leave of AbsenceLE - Left EarlyLO - LayoffM - Military LeaveN - No Cal/No ShowP - PersonalS - SuspensionT - TardyU - UnexcusedV - VacationX - Illness in the FamilyY - Floating HolidayZ - Last Day Worked
        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Q322585&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='Q322585']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='enduserquestionnaireform']/form/div[5]/div[2]/div/div[2]/div/div/div/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[11]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deny'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Approve'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
   </webElementXpaths>
</WebElementEntity>
