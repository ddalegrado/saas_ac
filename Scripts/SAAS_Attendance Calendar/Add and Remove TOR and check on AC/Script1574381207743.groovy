import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.sendKeys(findTestObject('Login/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'testnltd2019+4owner@gmail.com')

WebUI.sendKeys(findTestObject('Login/Page_HRdirect/input_PASSWORD_Password'), 'P@ssword123')

WebUI.click(findTestObject('Login/Page_HRdirect/button_exit_to_app Sign In to'))

WebUI.delay(20)

WebUI.maximizeWindow()

WebUI.verifyElementVisible(findTestObject('AC_Elements/AttendanceCalendar'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('TOR_Elements/TimeOffRequest_dashboard'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('TOR_Elements/TimeOffRequest_dashboard'))

WebUI.delay(10)

WebUI.click(findTestObject('TOR_Elements/RequestTimeOff_Dropdown'))

WebUI.delay(10)

WebUI.verifyElementVisible(findTestObject('TOR_Elements/RequestTimeOff_Menu'))

WebUI.click(findTestObject('TOR_Elements/EmployeeKaylynn_subject'))

WebUI.delay(20)

WebUI.setText(findTestObject('TOR_Elements/Time Off Request and Approval/ReasonForRequest_textbox'), 'Vacation Leave')

WebUI.click(findTestObject('TOR_Elements/Time Off Request and Approval/SelecType_Paid'))

WebUI.delay(3)

WebUI.click(findTestObject('TOR_Elements/Time Off Request and Approval/ListView_Tab'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('TOR_Elements/Time Off Request and Approval/SelectStartDate_DatePicker'), 3)

WebUI.setText(findTestObject('TOR_Elements/Time Off Request and Approval/SelectStartDate_DatePicker'), '01/25/2020')

not_run: WebUI.click(findTestObject('TOR_Elements/Time Off Request and Approval/SelectStartDate_DatePicker'))

not_run: WebUI.selectOptionByValue(findTestObject('TOR_Elements/Time Off Request and Approval/1DatePicker_Month'), '11', 
    false)

not_run: WebUI.selectOptionByValue(findTestObject('TOR_Elements/Time Off Request and Approval/1DatePicker_Year'), '2019', 
    false)

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('TOR_Elements/Time Off Request and Approval/1CalendarDate_Dec23-2019'))

WebUI.delay(5)

WebUI.click(findTestObject('TOR_Elements/Time Off Request and Approval/SelectEndDate_DatePicker'))

WebUI.setText(findTestObject('TOR_Elements/Time Off Request and Approval/SelectEndDate_DatePicker'), '01/27/2020')

not_run: WebUI.selectOptionByValue(findTestObject('TOR_Elements/Time Off Request and Approval/2DatePicker_Month'), '11', 
    false)

not_run: WebUI.selectOptionByValue(findTestObject('TOR_Elements/Time Off Request and Approval/2DatePicker_Year'), '2019', 
    false)

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('TOR_Elements/Time Off Request and Approval/2CalendarDate_Dec27-2019'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('TOR_Elements/Time Off Request and Approval/AbsenceCode_Dropdown'), 3)

WebUI.click(findTestObject('TOR_Elements/Time Off Request and Approval/AbsenceCode_Dropdown'))

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('TOR_Elements/Time Off Request and Approval/AbsenceCode_Dropdown'), '21', false)

WebUI.sendKeys(findTestObject('TOR_Elements/Time Off Request and Approval/Comment_Textbox'), 'This a test...')

WebUI.click(findTestObject('TOR_Elements/Time Off Request and Approval/Approve_Button'))

WebUI.delay(5)

WebUI.verifyElementVisible(findTestObject('TOR_Elements/Time Off Request and Approval/ApproveRequest_Modal'))

WebUI.click(findTestObject('TOR_Elements/Time Off Request and Approval/ApprovalRequest_Modal_Approve'))

WebUI.delay(10)

WebUI.click(findTestObject('TOR_Elements/HRDirect_Logo'))

WebUI.delay(10)

WebUI.click(findTestObject('AC_Elements/AttendanceCalendar'))

WebUI.delay(20)

WebUI.click(findTestObject('AC_Elements/AllActiveEmployees_Dropdown'))

WebUI.delay(5)

WebUI.click(findTestObject('AC_Elements/Add-Remove-TOR-CA/Employee_Kaylynn'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('AC_Elements/Add-Remove-TOR-CA/TOR_Dec27'), 0)

WebUI.click(findTestObject('AC_Elements/Add-Remove-TOR-CA/TOR_Dec27'))

WebUI.delay(5)

WebUI.verifyTextPresent('December 23, 2019', false)

WebUI.verifyElementVisible(findTestObject('AC_Elements/Add-Remove-TOR-CA/Approved_VacationLeave'))

WebUI.click(findTestObject('AC_Elements/Add Absence Event/AddAbsenceEvent_Close_Button'))

WebUI.delay(5)

WebUI.click(findTestObject('TOR_Elements/Dashboard_Button-TOR'))

WebUI.delay(5)

WebUI.click(findTestObject('TOR_Elements/TimeOfRequest_app'))

WebUI.delay(20)

WebUI.click(findTestObject('TOR_Elements/Time Off Request and Approval/TOR_tobedeleted'))

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('TOR_Elements/Time Off Request and Cancel/CancelRequest_button'), 0)

WebUI.click(findTestObject('TOR_Elements/Time Off Request and Cancel/CancelRequest_button'))

WebUI.delay(10)

WebUI.acceptAlert()

