import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://account.hrdirectappsstaging.com/')

WebUI.setText(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'halebmerin+35stage19qa@gmail.com')

WebUI.setText(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/input_PASSWORD_Password'), 'a111111!')

WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/button_exit_to_app Sign In to My Account'))

WebUI.maximizeWindow()

WebUI.delay(5)

'Launch TOR'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/a_TOR'))

WebUI.delay(5)

'-----TO BE REMOVED ONCE ISSUE IS FIXED----------'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/button_Get Started'))

WebUI.delay(2)

'Click Quick Add button'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/a_add_circle_outline Quick Add'))

WebUI.delay(2)

'Add First Name'
WebUI.setText(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/input__FirstName'), 'Anna')

'Add Last Name'
WebUI.setText(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/input__LastName'), 'Apples')

WebUI.delay(2)

'Click Save button'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/button_Save'))

WebUI.delay(3)

'Click \'Request Time Off\' button'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/Request Time Off_Dropdown'))

WebUI.delay(2)

'Select the newly added employee'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/Select_Anna Apples'))

WebUI.delay(2)

'provide TOR reason'
WebUI.setText(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/input__Request Reason'), 'TOR request ')

WebUI.delay(2)

'click Paid'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/radiobutton__Paid'))

WebUI.delay(2)

'Select Date'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/Dec_20'))

WebUI.delay(2)

'Input number of hours'
WebUI.setText(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/input_time'), '4')

WebUI.delay(2)

'input comment'
WebUI.setText(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/input_Comments'), 'for approval - then cancelled')

WebUI.delay(2)

'Scroll Reason dropdown'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/Scroll_absenceCode'))

WebUI.delay(2)

WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/Select_TOR_AbsenceCode'))

WebUI.delay(2)

'Click Approve'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/button_Approve'))

'Approve on Confirmation Modal'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/button_Approve_1'))

WebUI.delay(5)

'Go back to the Dashboard'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/img_SmartApps'))

'Launch AC app'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/a_AC'))

WebUI.delay(25)

'Click Employee dropdown'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/div_All Active Employees'))

'Select newly added employee with approve TOR'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/div_ApplesAnna'))

WebUI.delay(2)

'Go to date with approve TOR to verify'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/Dec_20_AC'))

WebUI.delay(5)

WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/i_cancel'))

'Close calendar window'
WebUI.delay(2)

'Go back to the Dashoard'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/SmartApps2'))

WebUI.delay(2)

'Launch TOR'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/a_TOR (1)'))

WebUI.delay(15)

'-----TO BE REMOVED ONCE ISSUE IS FIXED----------'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/button_Get Started'))

WebUI.delay(5)

'Locate Added employee'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/div_ApplesAnna_TORgrid'))

WebUI.delay(5)

'Cancel previously approved TOR'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/button_cancel Request'))

WebUI.delay(2)

'Confirm cancellation. TOR is no longer shown on the grid'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/button_Yes'))

WebUI.delay(5)

'Go to the Dashboard'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/img_SmartApps'))

WebUI.delay(2)

'Launch TOR'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/a_AC'))

WebUI.delay(15)

'Click Employee dropdown'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/div_All Active Employees'))

WebUI.delay(2)

'Locate Employee'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/div_ApplesAnna'))

WebUI.delay(2)

'Verify TOR is no longer shown'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/Dec_20_AC'))

WebUI.delay(5)

'close window'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/close_cancel'))

WebUI.delay(2)

'Go back to the Dashboard'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/img_SmartApps (1)'))

WebUI.delay(3)

'Launch Employee App'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/a_Employees'))

WebUI.delay(10)

'Locate added employee'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/Select_Anna'))

WebUI.delay(3)

WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/click_option'))

WebUI.delay(3)

'Delete employee'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/delete_Employee'))

WebUI.delay(3)

'confirm deletion'
WebUI.click(findTestObject('Approve and Cancel TOR and check on AC/Page_HRdirect/button_YES_delete'))

WebUI.delay(3)

