<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_arrow_drop_down</name>
   <tag></tag>
   <elementGuidId>0a8fb9e5-fbc3-4f3b-a5a1-4fe699a7c7b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav[@id='ui-shell-app-nav']/div[2]/div/ul[2]/li/separated-dropdown-list/div/div/a/i[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ui-shell-app-nav&quot;)/div[@class=&quot;right&quot;]/div[@class=&quot;app-nav-right&quot;]/ul[@class=&quot;app-nav-actions tor-app-nav-actions&quot;]/li[@class=&quot;action-link&quot;]/separated-dropdown-list[1]/div[@class=&quot;header-dropdown&quot;]/div[@class=&quot;dropdown&quot;]/a[@class=&quot;btn btn-icon btn-primary dropdown-toggle&quot;]/i[@class=&quot;material-icons dropdown-arrow&quot;][count(. | //i[@class = 'material-icons dropdown-arrow' and (text() = 'arrow_drop_down' or . = 'arrow_drop_down')]) = count(//i[@class = 'material-icons dropdown-arrow' and (text() = 'arrow_drop_down' or . = 'arrow_drop_down')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-icons dropdown-arrow</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>arrow_drop_down</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ui-shell-app-nav&quot;)/div[@class=&quot;right&quot;]/div[@class=&quot;app-nav-right&quot;]/ul[@class=&quot;app-nav-actions tor-app-nav-actions&quot;]/li[@class=&quot;action-link&quot;]/separated-dropdown-list[1]/div[@class=&quot;header-dropdown&quot;]/div[@class=&quot;dropdown&quot;]/a[@class=&quot;btn btn-icon btn-primary dropdown-toggle&quot;]/i[@class=&quot;material-icons dropdown-arrow&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//nav[@id='ui-shell-app-nav']/div[2]/div/ul[2]/li/separated-dropdown-list/div/div/a/i[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Request Time Off'])[1]/following::i[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='event'])[1]/following::i[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Team'])[1]/preceding::i[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Merin, Hanna'])[1]/preceding::i[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//a/i[2]</value>
   </webElementXpaths>
</WebElementEntity>
