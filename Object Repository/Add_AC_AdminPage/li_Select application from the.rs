<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Select application from the</name>
   <tag></tag>
   <elementGuidId>fb273d36-d997-4c85-8e86-d2cc63cbccdb</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>default-option</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                Select application from the list
                                                            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;customer-license-panel&quot;)/div[@class=&quot;splitview-content-scroller&quot;]/div[@class=&quot;splitview-content-overlay-main profile-content&quot;]/div[@class=&quot;tab-content&quot;]/div[@class=&quot;tab-pane fade in active&quot;]/form[@class=&quot;ng-untouched ng-pristine ng-valid&quot;]/div[@class=&quot;content-section&quot;]/div[@class=&quot;content-section-content&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;content-box&quot;]/div[@class=&quot;grid-form&quot;]/div[@class=&quot;form-group add-application-form-group&quot;]/div[@class=&quot;multiselect content-select-dropdown application-select-dropdown&quot;]/ul[1]/li[@class=&quot;default-option&quot;]</value>
   </webElementProperties>
</WebElementEntity>
