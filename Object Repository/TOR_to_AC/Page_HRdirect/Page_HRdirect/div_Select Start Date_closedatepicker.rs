<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Select Start Date_closedatepicker</name>
   <tag></tag>
   <elementGuidId>b4a22de5-9d3a-4f5c-afa9-921854c8be07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='calendarquestionnaireform']/div/div/div[4]/calendar-detail-component/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row noPadding</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
       
        
            
    Select Start Date*
    
        
    
    
        
    


            
    Select End Date*
    
        
    
    
        
    


            
                Total Time Requested: 
                
            
        

        
            
                Select Day(s) and Enter Time *
                
                    
                        
                            
                                
                                    
                                    
                                
                            
                            Tue 10/15/2019
                            
                                                            
                            
                        
                      
                
            
        

    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;calendarquestionnaireform&quot;)/div[@class=&quot;row noPadding&quot;]/div[@class=&quot;col-xs-12 col-sm-12&quot;]/div[@class=&quot;calendar-detail-view&quot;]/calendar-detail-component[1]/div[@class=&quot;request-details&quot;]/div[@class=&quot;row noPadding&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='calendarquestionnaireform']/div/div/div[4]/calendar-detail-component/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='S'])[26]/following::div[40]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='F'])[13]/following::div[41]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//calendar-detail-component/div/div</value>
   </webElementXpaths>
</WebElementEntity>
