import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

'Open SAAS test site'
WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

'Keyin registered email'
WebUI.setText(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'lou.gill000+CRTESTSMK12@gmail.com')

'Keyin valid pass'
WebUI.setEncryptedText(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/input_PASSWORD_Password'), 'vTJxSJ71VSN3xk74LjKEvw==')

'Click Sign to my account button'
WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/button_exit_to_app Sign In to'))

WebUI.maximizeWindow()

'Launch Time Off Request app'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/TimeOfRequest_app'))

not_run: WebUI.delay(5)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/i_arrow_drop_down'))

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/div_Baker Ali'))

not_run: WebUI.delay(5)

not_run: WebUI.setText(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/input__Q570404'), 'Vacation Leave')

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/input__Q476465'))

not_run: WebUI.delay(5)

not_run: WebUI.scrollToElement(findTestObject('TodaysEvent/Page_HRdirect/Page_HRdirect/span_15'), 9999999)

not_run: WebUI.click(findTestObject('TodaysEvent/Page_HRdirect/Page_HRdirect/span_15'))

not_run: WebUI.delay(4)

not_run: WebUI.setText(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/input_Tue_form-control ng-unto'), '12')

not_run: WebUI.delay(3)

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/select_Click to Select'), 
    '1398', true)

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/button_Approve'))

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/button_Approve_1'))

not_run: WebUI.delay(8)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/i_arrow_drop_down'))

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/div_Hardy Selma'))

not_run: WebUI.delay(5)

not_run: WebUI.setText(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/input__Q570404'), 'Personal')

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/span_Paid'))

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('TodaysEvent/Page_HRdirect/div_SH'))

not_run: WebUI.delay(3)

not_run: WebUI.scrollToElement(findTestObject('TodaysEvent/Page_HRdirect/Page_HRdirect/span_15'), 9999999)

not_run: WebUI.click(findTestObject('TodaysEvent/Page_HRdirect/Page_HRdirect/span_15'))

not_run: WebUI.setText(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/input_Tue_form-control ng-unto'), '8')

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/span_Who Else is Out (1)_caret'))

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('TodaysEvent/Page_HRdirect/div_SH                        _1'))

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/select_Click to Select'), 
    '1394', true)

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/button_Approve'))

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/button_Approve_2'))

not_run: WebUI.delay(5)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/i_apps'))

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/a_1'))

not_run: WebUI.delay(8)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/a_Try Beta'))

not_run: WebUI.delay(10)

not_run: WebUI.click(findTestObject('TodaysEvent/Page_HRdirect/span_Todays Events (2)'))

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/div_Vacation'))

not_run: WebUI.delay(5)

not_run: WebUI.scrollToElement(findTestObject('TodaysEvent/Page_HRdirect/Page_HRdirect/div_15'), 9999999)

not_run: WebUI.click(findTestObject('TodaysEvent/Page_HRdirect/Page_HRdirect/div_15'))

not_run: WebUI.delay(8)

not_run: WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/i_cancel'))

'Click Request Time Off dropdown employee list'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/RequestTimeOff_dropdown'))

WebUI.delay(2)

'Selecting Ali Baker from the list'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/Employee_Ali Baker'))

WebUI.delay(3)

'Key in Vacation in the Reason for request field'
WebUI.setText(findTestObject('TodaysEvent/Update-02282020/ReasonForRequest_field'), 'Vacation')

'enabling PAID type'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/Select Type_Paid'))

'Clicking List View tab'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/ListView_tab'))

'Scroling down to Absence Code dropdown'
WebUI.scrollToElement(findTestObject('TodaysEvent/Update-02282020/Absence Code_dropdown'), 0)

'Selecting Vacation among the list in Absence Code'
WebUI.selectOptionByValue(findTestObject('TodaysEvent/Update-02282020/Absence Code_dropdown'), '1398', true)

'Clicking Approve button'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/Approve_button'))

'Clicking Approve button from the confirmation modal'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/ApproveRequest modal_Approve button'))

WebUI.delay(5)

'Click Request Time Off dropdown employee list'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/RequestTimeOff_dropdown'))

WebUI.delay(2)

'Selecting Selma Hardy from the list'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/Employee_Selma Hardy'))

WebUI.delay(3)

'Key in Sick Leave in the Reason for request field'
WebUI.setText(findTestObject('TodaysEvent/Update-02282020/ReasonForRequest_field'), 'Sick Leave')

'enabling PAID type'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/Select Type_Paid'))

'Clicking List View tab'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/ListView_tab'))

'Scroling down to Absence Code dropdown'
WebUI.scrollToElement(findTestObject('TodaysEvent/Update-02282020/Absence Code_dropdown'), 0)

'Selecting Illness-Self among the list in Absence Code'
WebUI.selectOptionByValue(findTestObject('TodaysEvent/Update-02282020/Absence Code_dropdown'), '1386', true)

'Verifying if Who Else is out element is enabled/shown'
WebUI.verifyElementPresent(findTestObject('TodaysEvent/Update-02282020/Who Else is Out_dropdown'), 0)

'Checking if there is an employee listed in the dropdown'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/Who Else is Out_dropdown'))

WebUI.verifyTextPresent('Ali Baker', true)

'Clicking Approve button'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/Approve_button'))

'Clicking Approve button from the confirmation modal'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/ApproveRequest modal_Approve button'))

WebUI.delay(5)

'Clicking dashboard'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/Dashboard'))

WebUI.delay(2)

'Launch Attendance Calendar app'
WebUI.click(findTestObject('TodaysEvent/Update-02282020/Attendance Calendar_app'))

WebUI.delay(10)

'Verfiying if Event List is present'
WebUI.verifyElementPresent(findTestObject('TodaysEvent/Update-02282020/Todays Event_list'), 10)

WebUI.delay(5)

'Clicking Profile icon'
WebUI.click(findTestObject('Object Repository/TodaysEvent/Page_HRdirect/span_Bruno'))

WebUI.delay(3)

'Clicking Sign out'
WebUI.click(findTestObject('AddEditDelete/Page_HRdirect/span_Sign Out'))

WebUI.delay(5)

WebUI.closeBrowser()

