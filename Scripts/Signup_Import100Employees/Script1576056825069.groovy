import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

not_run: WebUI.maximizeWindow()

not_run: WebUI.click(findTestObject('Signup_import100Employees/a_SIGN UP'))

not_run: WebUI.setText(findTestObject('Signup_import100Employees/input_Email'), 'tstestp1+11crtest@gmail.com')

not_run: WebUI.setText(findTestObject('Signup_import100Employees/input_Password'), 'Password_3')

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('Signup_import100Employees/button_CREATE Account'))

not_run: WebUI.delay(5)

not_run: WebUI.maximizeWindow()

not_run: WebUI.click(findTestObject('Signup_import100Employees/button_Next'))

not_run: WebUI.setText(findTestObject('Signup_import100Employees/input_CompanyName'), 'prod-corp')

not_run: WebUI.delay(2)

not_run: WebUI.selectOptionByValue(findTestObject('Signup_import100Employees/select_Company_Industry'), '1', true)

not_run: WebUI.delay(1)

not_run: WebUI.selectOptionByValue(findTestObject('Signup_import100Employees/select_Company_Size'), '1', true)

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Signup_import100Employees/button_Next'))

not_run: WebUI.setText(findTestObject('Signup_import100Employees/input_FirstName'), 'owner')

not_run: WebUI.delay(1)

not_run: WebUI.setText(findTestObject('Signup_import100Employees/input_LastName'), 'owner')

not_run: WebUI.delay(1)

not_run: WebUI.setText(findTestObject('Signup_import100Employees/input_Phone Numbe'), '23423423423')

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Signup_import100Employees/div_Skip'))

not_run: WebUI.click(findTestObject('Signup_import100Employees/button_Save'))

WebUI.callTestCase(findTestCase('Create New Account'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.navigateToUrl('https://employees.hrdirectappsqa.com/')

WebUI.delay(2)

WebUI.click(findTestObject('Signup_import100Employees/a_Import'))

WebUI.delay(2)

WebUI.uploadFile(findTestObject('Signup_import100Employees/input_upload_files'), 'C:\\Users\\CZOcarol\\Katalon Studio\\Employees 2019_big account.xlsx')

WebUI.delay(20)

WebUI.navigateToUrl('https://employees.hrdirectappsqa.com/')

