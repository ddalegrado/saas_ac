<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_arrow_drop_down</name>
   <tag></tag>
   <elementGuidId>812b34fb-3bd8-40f1-9a0b-08f5b46707b0</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-icons avatar-menu-expand hidden-xs</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>arrow_drop_down</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ui-shell-top&quot;)/div[@class=&quot;right v-center&quot;]/div[@class=&quot;user-nav&quot;]/div[@class=&quot;dropdown&quot;]/a[@class=&quot;avatar-menu&quot;]/i[@class=&quot;material-icons avatar-menu-expand hidden-xs&quot;]</value>
   </webElementProperties>
</WebElementEntity>
