import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://account.hrdirectappsqa.com/')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'donatest1+SAAS395@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/input_PASSWORD_Password'),
	'3Z9vEXbPUsB23Jtp1+ckNg==')

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/button_exit_to_app Sign In to My Account'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/a'))

WebUI.delay(20)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/a_Settings'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/a_Calendar'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/div_All Active Employees'))

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/div_Five Five'))

'add event'
WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/button_Add Event'))

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/select_Select a code'),
	'1903', true)

WebUI.setText(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/input_Time_timeTaken'), '2')

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/a_Settings'))

WebUI.delay(10)

'disable code'
WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/span_Create New Code_control-indicator'))

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/a_Calendar'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/div_All Active Employees_1'))

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/div_Five Five'))

'verify disabled code is not shown'
WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/button_Add Event'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/div_Add Absence Event'))

WebUI.click(findTestObject('Settings_EnableDisableCode1/Page_HRdirect/select_Select a code'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/i_cancel'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/a_Settings'))

WebUI.delay(10)

'enable custom code'
WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/span_Edit_control-indicator'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/a_Calendar'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/div_All Active Employees_2'))

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/div_Five Five'))

'add event using custom code'
WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/button_Add Event'))

WebUI.delay(5)

WebUI.click(findTestObject('Settings_EnableDisableCode1/Page_HRdirect/select_Select a code'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/select_Select a code'),
	'1904', true)

WebUI.setText(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/input__datePickerComponent'),
	'11/20/2019')

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/div_FF'))

WebUI.setText(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/input_Time_timeTaken'), '3')

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/button_Save'))

WebUI.scrollToPosition(10, 20)

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/a_Settings'))

'disable custom code'
WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/span_Create New Code_control-indicator'))

'enable disabled code'
WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/span_Edit_control-indicator'))

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/a_Calendar'))

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/div_All Active Employees_3'))

'delete event added'
WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/div_Five Five'))

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/div_18'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/span_Five'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/span_Delete Event'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/button_Yes'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/span_19'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/span_Five'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/span_Delete Event'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/button_Yes'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/i_arrow_drop_down'))

WebUI.click(findTestObject('Object Repository/Settings_EnableDisableCode1/Page_HRdirect/span_Sign Out'))

WebUI.closeBrowser()