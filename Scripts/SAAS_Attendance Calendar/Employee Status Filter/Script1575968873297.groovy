import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://account.hrdirectappsqa.com/')

'Launching SAAS site'
WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

'Keyin registered email address in Email field'
WebUI.setText(findTestObject('Employee Status Filter/input_EMAIL ADDRESS_Email'), 'tstestp1+38QA@gmail.com')

'Keyin valid password in Passoword field'
WebUI.setText(findTestObject('Employee Status Filter/input_PASSWORD_Password'), 'Password_3')

'Clicking Signin to my account button'
WebUI.click(findTestObject('Employee Status Filter/button_exit_to_app Sign In to My Account'))

WebUI.delay(5)

WebUI.maximizeWindow()

'Clicking Attendance Calendar application'
WebUI.click(findTestObject('Employee Status Filter/Launch_AC'))

WebUI.delay(20)

'Click All Employee dropdown'
WebUI.click(findTestObject('Employee Status Filter/div_All Active Employees'))

WebUI.delay(3)

'Select One Attitude fromt he employeee list'
WebUI.click(findTestObject('Employee Status Filter/Emp_AttitudeOne'))

WebUI.delay(3)

'Click Year-Moth toggle'
WebUI.click(findTestObject('Employee Status Filter/Year-Month_Toggle'))

WebUI.delay(3)

'Click All Employee dropdown'
WebUI.click(findTestObject('Employee Status Filter/Emp_Dropdown'))

WebUI.delay(3)

'Select Two Beauty from the employeee list'
WebUI.click(findTestObject('Employee Status Filter/employee_BeautyTwo'))

WebUI.delay(3)

'Click Year-Month toggle'
WebUI.click(findTestObject('Employee Status Filter/Year-Month_Toggle'))

WebUI.delay(3)

'Click All Employee dropdown'
WebUI.click(findTestObject('Employee Status Filter/div_All Active Employees'))

WebUI.delay(3)

'Scroll to employee name 10, Ten'
WebUI.scrollToElement(findTestObject('Employee Status Filter/emp_Inactive_Ten'), 3)

WebUI.delay(3)

'Select 10 Ten from the employee list'
WebUI.click(findTestObject('Employee Status Filter/emp_Inactive_Ten'))

WebUI.delay(3)

'Clicking Profile icon'
WebUI.click(findTestObject('AC_Elements/Profile_dropdown'))

WebUI.delay(3)

'Clicking sign out button'
WebUI.click(findTestObject('AC_Elements/SignOut_button'))

WebUI.delay(5)

'Verify logout is successful by checking login form in the login page'
WebUI.verifyElementPresent(findTestObject('AC_Elements/Login_Panel'), 3)

'Closing browser'
WebUI.closeBrowser()

