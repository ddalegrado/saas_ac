import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.delay(5)

WebUI.click(findTestObject('New Registration/Sign-up'))

WebUI.delay(5)

WebUI.sendKeys(findTestObject('New Registration/EmailAddress_field'), ('ians_' + new Date().getTime()) + '@yopmail.com')

WebUI.sendKeys(findTestObject('New Registration/Password_field'), 'P@ssword123')

WebUI.click(findTestObject('FirstRun-AC/SignUp-CreateAccount_button'))

not_run: WebUI.setText(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/input_EMAIL ADDRESS_Email'), ('ian_' + 
    ts) + '@yopmail.com')

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/input_PASSWORD_Password'), 'vTJxSJ71VSN3xk74LjKEvw==')

not_run: WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/button_exit_to_app Sign In to'))

WebUI.maximizeWindow()

WebUI.delay(3)

not_run: WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/div_Attendance Calendar'))

not_run: WebUI.acceptAlert()

WebUI.click(findTestObject('Object Repository/Create New Account/Page_HRdirect/button_Next'))

WebUI.setText(findTestObject('Object Repository/Create New Account/Page_HRdirect/input_CompanyName'), 'AC Company')

WebUI.selectOptionByValue(findTestObject('Object Repository/Create New Account/Page_HRdirect/select_Industry'), '2', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Create New Account/Page_HRdirect/select_CompanySize'), '1', 
    true)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Create New Account/Page_HRdirect/button_Next'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Create New Account/Page_HRdirect/input_First Name_FirstName'), 'Owner')

WebUI.setText(findTestObject('Object Repository/Create New Account/Page_HRdirect/input_Last Name_LastName'), 'Owner')

WebUI.setText(findTestObject('Object Repository/Create New Account/Page_HRdirect/input_Phone Number_Phone'), '(876) 543-2109')

WebUI.delay(3)

WebUI.doubleClick(findTestObject('Create New Account/Page_HRdirect/Carousel bottom'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Create New Account/Page_HRdirect/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('TOR_Elements/HRDirect_Logo'))

WebUI.delay(3)

WebUI.click(findTestObject('FirstRun-AC/Un-Purchased-AC'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/button_Get Started'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/input__address1'), '2616 Pyramid Valley Road')

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/input__city'), 'Waverly')

WebUI.delay(1)

WebUI.selectOptionByValue(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/select_Select A State'), 'IA', true)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/input__postal_code'), '50677')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/label_CREDIT CARD NUMBER'))

WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/div_CREDIT CARD NUMBER'))

WebUI.setText(findTestObject('FirstRun-AC/Page_HRdirect/inputrecurly-hosted-field-inpu'), '4012 8888 8888 1881')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/label_EXPIRATION MONTH'))

WebUI.setText(findTestObject('FirstRun-AC/Page_HRdirect/inputrecurly-hosted-field-inpu (1)'), '12')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/label_Expiration Year'))

WebUI.setText(findTestObject('FirstRun-AC/Page_HRdirect/inputrecurly-hosted-field-inpu (2)'), '2025')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/label_CVV'))

WebUI.setText(findTestObject('FirstRun-AC/Page_HRdirect/inputrecurly-hosted-field-inpu (3)'), '110')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/button_shopping_cart Pay Now'))

WebUI.delay(60)

WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/button_Next'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/span_Vacation  Sick Separate b'))

WebUI.click(findTestObject('FirstRun-AC/Page_HRdirect/button_Next(2)'))

WebUI.delay(3)

WebUI.click(findTestObject('FirstRun-AC/Page_HRdirect/span_Yes'))

WebUI.click(findTestObject('FirstRun-AC/Page_HRdirect/button_Next(2)'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_HRdirect/button_Complete'))

WebUI.delay(10)

not_run: WebUI.click(findTestObject('Object Repository/FirstRun-AC/Page_Attendance-Calendar-IV.pdf - H/a_Try Beta'))

WebUI.delay(5)

not_run: WebUI.click(findTestObject('TOR_Elements/HRDirect_Logo'))

not_run: WebUI.verifyElementPresent(findTestObject('AC_Elements/AttendanceCalendar'), 0)

WebUI.click(findTestObject('FirstRun-AC/Page_HRdirect/i_arrow_drop_down'))

WebUI.delay(3)

WebUI.click(findTestObject('FirstRun-AC/Page_HRdirect/span_Sign Out'))

WebUI.delay(5)

WebUI.closeBrowser()

