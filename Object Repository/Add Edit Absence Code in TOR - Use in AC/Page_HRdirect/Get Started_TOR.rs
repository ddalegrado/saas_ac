<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Get Started_TOR</name>
   <tag></tag>
   <elementGuidId>9ec26dec-3b2a-4e73-9ac2-14a07e48ce81</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[13]/div[5]/div[3]/section[1]/div/div/div[2]/div[1]/div[2]/div/button[count(. | //button[@class = 'btn btn-cta subscribeButton']) = count(//button[@class = 'btn btn-cta subscribeButton'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[13]/div[5]/div[3]/section[1]/div/div/div[2]/div[1]/div[2]/div/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-cta subscribeButton</value>
   </webElementProperties>
</WebElementEntity>
