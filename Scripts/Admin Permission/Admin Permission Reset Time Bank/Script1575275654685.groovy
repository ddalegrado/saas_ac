import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Admin Permission_Reset Time bank/Page_HRdirect/input_EMAIL ADDRESS_Email'), 
    'lou.gill000+CRTESTAC4ad@gmail.com')

WebUI.setEncryptedText(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/input_PASSWORD_Password'), 'vTJxSJ71VSN3xk74LjKEvw==')

WebUI.click(findTestObject('Admin Permission/Page_HRdirect/button_exit_to_app Sign In to'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Admin Permission_Reset Time bank/Page_HRdirect/a'))

WebUI.delay(25)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/span_No events today_calendar-toggle__switch'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Admin Permission_Reset Time bank/Page_HRdirect/div_All Active Employees'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/div_SE                                                                                                        English Scott'))

WebUI.delay(10)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/div_add'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/div_Select a code'))

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/a_P                                                                     Personal'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/select_VacationSickNone'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Admin Permission_Reset Time bank/Page_HRdirect/input_Time_timeTaken'), '20')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission_Reset Time bank/Page_HRdirect/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/div_add'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/div_Select a code'))

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/a_P                                                                     Personal'))

WebUI.delay(10)

WebUI.setText(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/input_Time_timeTaken'), '9')

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/select_VacationSickNone'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission_Reset Time bank/Page_HRdirect/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/div_add'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/div_Select a code'))

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/a_P                                                                     Personal'))

WebUI.delay(5)

WebUI.setText(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/input_Time_timeTaken'), '21')

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/button_Save'))

WebUI.delay(10)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/i_more_vert'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/a_Edit Hours'))

WebUI.delay(5)

WebUI.setText(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/input_Starting Hours_vacationStartTime'), 
    '41')

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/i_more_vert'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/a_Reset Starting Hours'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/i_more_vert'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/a_Edit Hours'))

WebUI.delay(5)

WebUI.setText(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/input_Starting Hours_vacationStartTime'), 
    '20')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission_Reset Time bank/Page_HRdirect/button_Save_2'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/i_more_vert'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/a_Reset Starting Hours'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/i_more_vert'))

WebUI.delay(5)

WebUI.setText(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/input_Starting Hours_vacationStartTime'), 
    '20')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission_Reset Time bank/Page_HRdirect/button_Save_4'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/i_more_vert'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/a_Edit Hours'))

WebUI.delay(5)

WebUI.setText(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/input_Starting Hours_vacationStartTime'), 
    '25')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Admin Permission_Reset Time bank/Page_HRdirect/button_Save_5'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/i_more_vert'))

WebUI.delay(5)

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/Page_HRdirect/a_Reset Starting Hours'))

WebUI.click(findTestObject('Admin Permission_Reset Time bank/Page_HRdirect/span_Sign Out'))

WebUI.closeBrowser()

