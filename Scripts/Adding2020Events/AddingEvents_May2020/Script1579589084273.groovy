import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

'select month MAY'
WebUI.click(findTestObject('Adding2020Events/span_May'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Military Leave'
WebUI.click(findTestObject('Adding2020Events/span_Military Leave'))

WebUI.delay(3)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/02/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - No CallNo Show'
WebUI.click(findTestObject('Adding2020Events/span_No CallNo Show'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/03/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Personal'
WebUI.click(findTestObject('Adding2020Events/span_Personal'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/04/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Military Leave'
WebUI.click(findTestObject('Adding2020Events/span_Military Leave'))

WebUI.delay(3)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/05/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - No CallNo Show'
WebUI.click(findTestObject('Adding2020Events/span_No CallNo Show'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/06/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Personal'
WebUI.click(findTestObject('Adding2020Events/span_Personal'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/07/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Military Leave'
WebUI.click(findTestObject('Adding2020Events/span_Military Leave'))

WebUI.delay(3)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/08/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - No CallNo Show'
WebUI.click(findTestObject('Adding2020Events/span_No CallNo Show'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/09/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Personal'
WebUI.click(findTestObject('Adding2020Events/span_Personal'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/10/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Military Leave'
WebUI.click(findTestObject('Adding2020Events/span_Military Leave'))

WebUI.delay(3)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/11/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - No CallNo Show'
WebUI.click(findTestObject('Adding2020Events/span_No CallNo Show'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/12/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Personal'
WebUI.click(findTestObject('Adding2020Events/span_Personal'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/13/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Military Leave'
WebUI.click(findTestObject('Adding2020Events/span_Military Leave'))

WebUI.delay(3)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/14/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - No CallNo Show'
WebUI.click(findTestObject('Adding2020Events/span_No CallNo Show'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/15/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Personal'
WebUI.click(findTestObject('Adding2020Events/span_Personal'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/16/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Military Leave'
WebUI.click(findTestObject('Adding2020Events/span_Military Leave'))

WebUI.delay(3)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/17/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - No CallNo Show'
WebUI.click(findTestObject('Adding2020Events/span_No CallNo Show'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/18/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Personal'
WebUI.click(findTestObject('Adding2020Events/span_Personal'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/19/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Military Leave'
WebUI.click(findTestObject('Adding2020Events/span_Military Leave'))

WebUI.delay(3)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/20/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - No CallNo Show'
WebUI.click(findTestObject('Adding2020Events/span_No CallNo Show'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/21/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Personal'
WebUI.click(findTestObject('Adding2020Events/span_Personal'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/22/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Military Leave'
WebUI.click(findTestObject('Adding2020Events/span_Military Leave'))

WebUI.delay(3)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/23/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Object Repository/Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - No CallNo Show'
WebUI.click(findTestObject('Adding2020Events/span_No CallNo Show'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'Sick', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/24/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

'click Add Event from the Calendar'
WebUI.click(findTestObject('Adding2020Events/div_add'))

WebUI.delay(3)

'open Code dropdown'
WebUI.click(findTestObject('Object Repository/Adding2020Events/span_Select a code'))

'select Absence Code - Personal'
WebUI.click(findTestObject('Adding2020Events/span_Personal'))

WebUI.delay(3)

'select Time Bank'
WebUI.selectOptionByLabel(findTestObject('Object Repository/Adding2020Events/select_VacationSickNone'), 'None', true)

'enter date'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input__datePickerComponent'), '05/25/2020')

'enter time'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/input_Time_timeTaken'), '1')

'enter notes'
WebUI.setText(findTestObject('Object Repository/Adding2020Events/textarea_Notes_notes'), 'test')

'click Save button'
WebUI.click(findTestObject('Object Repository/Adding2020Events/button_Save'))

WebUI.delay(3)

