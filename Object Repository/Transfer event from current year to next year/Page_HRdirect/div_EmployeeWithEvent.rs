<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_EmployeeWithEvent</name>
   <tag></tag>
   <elementGuidId>8abd6d78-b8fc-4b4a-9b3f-42234eb2d95c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div[1]/platform-apps-main/div/ng-component/events-list-panel-component/div/div/div[2]/div/div/div/div/div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='absenceeventpanel']/div/form/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tab-content</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                            
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                        
                                                        
                                                        
                                                            AA
                                                        
                                                    
                                                    Admin, Admin
                                                
                                                
                                                
                                                
                                                    Absence Code*
                                                    
                                                        
                                                            
                                                                A
                                                            
                                                            Additional Hours
                                                        
                                                        
                                                            
                                                                
                                                                    
                                                                        A
                                                                     Additional Hours
                                                                
                                                            
                                                                
                                                                    
                                                                        B
                                                                     Bereavement
                                                                
                                                            
                                                                
                                                                    
                                                                        C
                                                                     Partial Hours Worked
                                                                
                                                            
                                                                
                                                                    
                                                                        D
                                                                     Doctor's Appointment
                                                                
                                                            
                                                                
                                                                    
                                                                        E
                                                                     Excused
                                                                
                                                            
                                                                
                                                                    
                                                                        F
                                                                     FMLA
                                                                
                                                            
                                                                
                                                                    
                                                                        G
                                                                     Injury on Job
                                                                
                                                            
                                                                
                                                                    
                                                                        H
                                                                     Holiday
                                                                
                                                            
                                                                
                                                                    
                                                                        I
                                                                     Illness-Self
                                                                
                                                            
                                                                
                                                                    
                                                                        J
                                                                     Jury Duty
                                                                
                                                            
                                                                
                                                                    
                                                                        K
                                                                     Termination
                                                                
                                                            
                                                                
                                                                    
                                                                        L
                                                                     Leave of Absence
                                                                
                                                            
                                                                
                                                                    
                                                                        LE
                                                                     Left Early
                                                                
                                                            
                                                                
                                                                    
                                                                        LO
                                                                     Layoff
                                                                
                                                            
                                                                
                                                                    
                                                                        M
                                                                     Military Leave
                                                                
                                                            
                                                                
                                                                    
                                                                        N
                                                                     No Cal/No Show
                                                                
                                                            
                                                                
                                                                    
                                                                        P
                                                                     Personal
                                                                
                                                            
                                                                
                                                                    
                                                                        S
                                                                     Suspension
                                                                
                                                            
                                                                
                                                                    
                                                                        T
                                                                     Tardy
                                                                
                                                            
                                                                
                                                                    
                                                                        U
                                                                     Unexcused
                                                                
                                                            
                                                                
                                                                    
                                                                        V
                                                                     Vacation
                                                                
                                                            
                                                                
                                                                    
                                                                        X
                                                                     Illness in the Family
                                                                
                                                            
                                                                
                                                                    
                                                                        Y
                                                                     Floating Holiday
                                                                
                                                            
                                                                
                                                                    
                                                                        Z
                                                                     Last Day Worked
                                                                
                                                            
                                                        
                                                    
                                                    
                                                    
                                                        
                                                    
                                                
                                                
                                                    Time Bank
                                                    
                                                        
                                                            PTONone
                                                        
                                                    
                                                
                                                
    DATE*
    
        
    
    
        
    


                                                
                                                    Time
                                                    
                                                        
                                                    
                                                    
                                                        
                                                    
                                                
                                                
                                                    Notes
                                                    
                                                                                                                
                                                        4/155
                                                    
                                                

                                                
                                                    delete_forever
                                                    Delete Event
                                                

                                            
                                        
                                    
                                
                            
                        
                    
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div[1]/platform-apps-main/div/ng-component/events-list-panel-component/div/div/div[2]/div/div/div/div/div</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='absenceeventpanel']/div/form/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='cancel'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit Absence Event'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/form/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
