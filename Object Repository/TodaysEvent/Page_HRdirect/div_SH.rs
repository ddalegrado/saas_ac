<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_SH</name>
   <tag></tag>
   <elementGuidId>23d126b6-3f30-44f8-b11f-d47b0444cffa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//timeoff-requests-main[@id='wrapper']/div/ng-component/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>timeoff-view-container timeoff-form content-main-inner padder-90 has-footerbar</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
        
            
                
                    
                        
                            
                                
                                    
                                    
                                        SH
                                    
                                
                                
                                    Selma Hardy
                                     Time Off Requests (0)arrow_drop_down
                                
                            
                            
                                
                                
                                    
                                         No time off requests exist for this employee.
                                    
                                
                            
                        
                    
                
            
        
                    
            
                
                    
    
        
        
            Who Else is Out (0)
        
        
            
        
    



                                
                
                
            
        
    
    
        

            
                
                    
                        
    Status: New
    
    


                        

    
    
        
        
            
                
                    
                        
                            
                                
                                    
                                        
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        Time Off Request and Approval
                                    
                                
                            
                        
                        * Required
                    
                
            
        
        
        
        
            
            
                Employee Information
            
            

            
                
                    
                    
                        
                        
                            
    
    
    
    
    
    
    
    
        
        
            First Name *
        

        
        
        
        
        
        
        
         
        
        
        
        
        
        
        
        
         
        
        
        
        
        
            
        
        
        
        
        

        
        
        
        
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                        
                        
                            
    
    
    
    
    
    
    
    
        
        
            Last Name *
        

        
        
        
        
        
        
        
         
        
        
        
        
        
        
        
        
         
        
        
        
        
        
            
        
        
        
        
        

        
        
        
        
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                        
                        
                            
    
    
    
    
    
    
    
    
        
        
            Created on 
        

        
        
        
        
        
        
        
         
        
        
        
        
        
        
        
        
         
        
        
        
        
        
            
        
        
        
        
        

        
        
        
        
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                        
                        
                            
    
    
    
    
    
    
    
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                        
                        
                            
    
    
    
    
    
    
    
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                        
                        
                            
    
    
    
    
    
    
    
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                    
                
            
        
            
            
                Request Details
            
            

            
                
                    
                    
                        
                        
                            
    
    
    
    
    
    
    
    
        
        
            Reason for Request: *
        

        
        
        
        
        
        
        
         
        
        
        
        
        
        
        
        
         
        
        
        
        
        
            
        
        
        
        
        

        
        
        
        
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                        
                        
                            
    
    
    
    
    
    
    
    

    
    
    

    
    
    
    
    
        Select Type *
        
            
                
                Paid
            
                
                Unpaid
            
        

        
            
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                    
                
            
        
            
            
                Request Dates
            
            

            
                
                    
                    
                        
                        
                            
    
    
    
    
    
    
    
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
            
                
                    
                    CALENDAR VIEW

                    
                    LIST VIEW

                    
                
            

            
                
            

            
                
                    
                        
                            
                            
                            
                                2019
                            
                        
                    
                    
                        
                             Approved
                             Pending
                             Current
                             Legal Public Holidays
                        
                    
                
                
                    
                        
                                            

    
        January
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
                
                    
                
                    
                
            
            
                1
            
                2
            
                3
            
                4
            
                5
            
        
            
            
                6
            
                7
            
                8
            
                9
            
                10
            
                11
            
                12
            
        
            
            
                13
            
                14
            
                15
            
                16
            
                17
            
                18
            
                19
            
        
            
            
                20
            
                21
            
                22
            
                23
            
                24
            
                25
            
                26
            
        
            
            
                27
            
                28
            
                29
            
                30
            
                31
            
        
    



                        
                                            

    
        February
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
                
                    
                
                    
                
                    
                
                    
                
                    
                
            
            
                1
            
                2
            
        
            
            
                3
            
                4
            
                5
            
                6
            
                7
            
                8
            
                9
            
        
            
            
                10
            
                11
            
                12
            
                13
            
                14
            
                15
            
                16
            
        
            
            
                17
            
                18
            
                19
            
                20
            
                21
            
                22
            
                23
            
        
            
            
                24
            
                25
            
                26
            
                27
            
                28
            
        
    



                        
                                            

    
        March
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
                
                    
                
                    
                
                    
                
                    
                
                    
                
            
            
                1
            
                2
            
        
            
            
                3
            
                4
            
                5
            
                6
            
                7
            
                8
            
                9
            
        
            
            
                10
            
                11
            
                12
            
                13
            
                14
            
                15
            
                16
            
        
            
            
                17
            
                18
            
                19
            
                20
            
                21
            
                22
            
                23
            
        
            
            
                24
            
                25
            
                26
            
                27
            
                28
            
                29
            
                30
            
        
            
            
                31
            
        
    



                        
                                            

    
        April
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
                
                    
                
            
            
                1
            
                2
            
                3
            
                4
            
                5
            
                6
            
        
            
            
                7
            
                8
            
                9
            
                10
            
                11
            
                12
            
                13
            
        
            
            
                14
            
                15
            
                16
            
                17
            
                18
            
                19
            
                20
            
        
            
            
                21
            
                22
            
                23
            
                24
            
                25
            
                26
            
                27
            
        
            
            
                28
            
                29
            
                30
            
        
    



                        
                                            

    
        May
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
                
                    
                
                    
                
                    
                
            
            
                1
            
                2
            
                3
            
                4
            
        
            
            
                5
            
                6
            
                7
            
                8
            
                9
            
                10
            
                11
            
        
            
            
                12
            
                13
            
                14
            
                15
            
                16
            
                17
            
                18
            
        
            
            
                19
            
                20
            
                21
            
                22
            
                23
            
                24
            
                25
            
        
            
            
                26
            
                27
            
                28
            
                29
            
                30
            
                31
            
        
    



                        
                                            

    
        June
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
                
                    
                
                    
                
                    
                
                    
                
                    
                
                    
                
            
            
                1
            
        
            
            
                2
            
                3
            
                4
            
                5
            
                6
            
                7
            
                8
            
        
            
            
                9
            
                10
            
                11
            
                12
            
                13
            
                14
            
                15
            
        
            
            
                16
            
                17
            
                18
            
                19
            
                20
            
                21
            
                22
            
        
            
            
                23
            
                24
            
                25
            
                26
            
                27
            
                28
            
                29
            
        
            
            
                30
            
        
    



                        
                                            

    
        July
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
                
                    
                
            
            
                1
            
                2
            
                3
            
                4
            
                5
            
                6
            
        
            
            
                7
            
                8
            
                9
            
                10
            
                11
            
                12
            
                13
            
        
            
            
                14
            
                15
            
                16
            
                17
            
                18
            
                19
            
                20
            
        
            
            
                21
            
                22
            
                23
            
                24
            
                25
            
                26
            
                27
            
        
            
            
                28
            
                29
            
                30
            
                31
            
        
    



                        
                                            

    
        August
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
                
                    
                
                    
                
                    
                
                    
                
            
            
                1
            
                2
            
                3
            
        
            
            
                4
            
                5
            
                6
            
                7
            
                8
            
                9
            
                10
            
        
            
            
                11
            
                12
            
                13
            
                14
            
                15
            
                16
            
                17
            
        
            
            
                18
            
                19
            
                20
            
                21
            
                22
            
                23
            
                24
            
        
            
            
                25
            
                26
            
                27
            
                28
            
                29
            
                30
            
                31
            
        
    



                        
                                            

    
        September
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
            
                1
            
                2
            
                3
            
                4
            
                5
            
                6
            
                7
            
        
            
            
                8
            
                9
            
                10
            
                11
            
                12
            
                13
            
                14
            
        
            
            
                15
            
                16
            
                17
            
                18
            
                19
            
                20
            
                21
            
        
            
            
                22
            
                23
            
                24
            
                25
            
                26
            
                27
            
                28
            
        
            
            
                29
            
                30
            
        
    



                        
                                            

    
        October
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
                
                    
                
                    
                
            
            
                1
            
                2
            
                3
            
                4
            
                5
            
        
            
            
                6
            
                7
            
                8
            
                9
            
                10
            
                11
            
                12
            
        
            
            
                13
            
                14
            
                15
            
                16
            
                17
            
                18
            
                19
            
        
            
            
                20
            
                21
            
                22
            
                23
            
                24
            
                25
            
                26
            
        
            
            
                27
            
                28
            
                29
            
                30
            
                31
            
        
    



                        
                                            

    
        November
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
                
                    
                
                    
                
                    
                
                    
                
                    
                
            
            
                1
            
                2
            
        
            
            
                3
            
                4
            
                5
            
                6
            
                7
            
                8
            
                9
            
        
            
            
                10
            
                11
            
                12
            
                13
            
                14
            
                15
            
                16
            
        
            
            
                17
            
                18
            
                19
            
                20
            
                21
            
                22
            
                23
            
        
            
            
                24
            
                25
            
                26
            
                27
            
                28
            
                29
            
                30
            
        
    



                        
                                            

    
        December
    
    
        S
        M
        T
        W
        T
        F
        S
    

    
        
            
            
                1
            
                2
            
                3
            
                4
            
                5
            
                6
            
                7
            
        
            
            
                8
            
                9
            
                10
            
                11
            
                12
            
                13
            
                14
            
        
            
            
                15
            
                16
            
                17
            
                18
            
                19
            
                20
            
                21
            
        
            
            
                22
            
                23
            
                24
            
                25
            
                26
            
                27
            
                28
            
        
            
            
                29
            
                30
            
                31
            
        
    



                        
                    
                
            
                      
            
    
       
        
            
    Select Start Date*
    
        
    
    
        
    


            
    Select End Date*
    
        
    
    
        
    


            
                Total Time Requested: 
                
            
        

        
            
                Select Day(s) and Enter Time *
                
                      
                
            
        

    


        
        
    


    

    
    
    




                        
                        
                        
                        
                        
                    
                    
                
            
        
            
            
                Request Authorization (Manager/Supervisor Only)
            
            

            
                
                    
                    
                        
                        
                            
    
    
    
    
    
    
    
    
        
        
            Comments 
        

        
        
        
        
        
        
        
         
        
        
        
        
        
        
        
        
         
        
        
        
        
        
            
        
        
        
        
        

        
        
        
        
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                        
                        
                            
    
    
        Absence Code (if approving request) *
        
             Click to Select 
            A - Additional HoursB - BereavementC - Partial Hours WorkedD - Doctor's AppointmentE - ExcusedF - FMLAG - Injury on JobH - HolidayI - Illness-SelfJ - Jury DutyK - TerminationL - Leave of AbsenceLE - Left EarlyLO - LayoffM - Military LeaveN - No Cal/No ShowP - PersonalS - SuspensionT - TardyU - UnexcusedV - VacationX - Illness in the FamilyY - Floating HolidayZ - Last Day Worked
        
        
            
        
    
    
    
    
    
    
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                        
                        
                            
    
    
    
    
    
    
    
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                        
                        
                            
    
    
    
    
    
    
    
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                        
                        
                            
    
    
    
    
    
    
    
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    




                        
                        
                        
                        
                        
                    
                    
                
            
        
        
        
        

        
            
                
                       
                
            
        
        
    







    

    
    
            
            
                
                   
                    
                        Deny
                    
                        Approve
                    
                
            
            

            
            
    


                    
                
            
        
    
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wrapper&quot;)/div[@class=&quot;content-main-scroller&quot;]/ng-component[1]/div[@class=&quot;timeoff-view-container timeoff-form content-main-inner padder-90 has-footerbar&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//timeoff-requests-main[@id='wrapper']/div/ng-component/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home Phone'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Work Extension'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//ng-component/div</value>
   </webElementXpaths>
</WebElementEntity>
