<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_TOR</name>
   <tag></tag>
   <elementGuidId>3fb322c9-cde6-4c75-ac69-65df8ad245fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='dashboard-apps-card']/div[2]/div/div/div[3]/div/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dashboard-apps-card&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12 col-md-12&quot;]/div[@class=&quot;row apps&quot;]/div[3]/div[@class=&quot;col-xs-6 col-sm-4 col-md-4 col-lg-3 col-fhd-5ths app-card active available not-new&quot;]/div[@class=&quot;app-card-icon&quot;]/a[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dashboard-apps-card']/div[2]/div/div/div[3]/div/div/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/a</value>
   </webElementXpaths>
</WebElementEntity>
