<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_FF</name>
   <tag></tag>
   <elementGuidId>01919a44-6484-4bce-b1cd-33f5c97ad5fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='absenceeventpanel']/div/form/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tab-content</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                            
                                
                                    
                                        
                                            
                                                
                                                    
                                                        
                                                        
                                                        
                                                        
                                                            FF
                                                        
                                                    
                                                    Five, Five
                                                
                                                
                                                    Time Bank
                                                    
                                                        
                                                            PTONone
                                                        
                                                    
                                                
                                                
                                                    Absence Code*
                                                    
                                                        
                                                        
                                                            Select a code
                                                            AH-All Hands MeetingB-BereavementC-Partial Hours WorkedD-Doctor's AppointmentE-ExcusedF-FMLAG-Injury on JobH-HolidayI-Illness-SelfJ-Jury DutyK-TerminationL-Leave of AbsenceLE-Left EarlyLO-LayoffM-Military LeaveN-No Cal/No ShowP-PersonalS-SuspensionT-TardyU-UnexcusedV-VacationX-Illness in the FamilyY-Floating HolidayZ-Last Day Worked
                                                        
                                                    
                                                    
                                                        
                                                    
                                                

                                                
    DATE*
    
        
    
    
        
    


                                                
                                                    Time
                                                    
                                                        
                                                    
                                                    
                                                        
                                                    
                                                
                                                
                                                    Notes
                                                    
                                                                                                                
                                                        0/155
                                                    
                                                
                                               
                                                
                                               
                                            
                                        
                                    
                                
                            
                        
                    
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;absenceeventpanel&quot;)/div[@class=&quot;splitview-content-scroller&quot;]/form[@class=&quot;ng-valid ng-dirty ng-touched&quot;]/div[@class=&quot;profile-content&quot;]/div[@class=&quot;tab-content&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='absenceeventpanel']/div/form/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='cancel'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Absence Event'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div/form/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
