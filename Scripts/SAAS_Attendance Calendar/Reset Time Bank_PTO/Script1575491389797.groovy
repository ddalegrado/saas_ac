import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://account.hrdirectappsqa.com/')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/input_EMAIL ADDRESS_Email'), 'donatest1+SAAS412@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/input_PASSWORD_Password'), '3Z9vEXbPUsB23Jtp1+ckNg==')

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/button_exit_to_app Sign In to My Account'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/a'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/div_All Active Employees'))

WebUI.delay(10)

WebUI.click(findTestObject('Reset Time Bank_PTO/Page_HRdirect/div_EE                                                                                                        Employee01 Employee01'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/button_year'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/a_Month'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_more_vert'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_more_vert'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/input_Starting Hours_vacationStartTime'))

WebUI.setText(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/input_Starting Hours_vacationStartTime'), 
    '10')

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/button_Save'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_more_vert'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/input_Starting Hours_vacationStartTime'))

WebUI.setText(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/input_Starting Hours_vacationStartTime'), 
    '15')

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/button_Save'))

WebUI.delay(10)

WebUI.click(findTestObject('Reset Time Bank_PTO/Page_HRdirect/button_Add Event'))

not_run: WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_add'))

'test'
WebUI.click(findTestObject('Reset Time Bank_PTO/Page_HRdirect/div_Select a code'))

WebUI.delay(3)

'test'
WebUI.click(findTestObject('Reset Time Bank_PTO/Page_HRdirect/span_Additional Hours'))

not_run: WebUI.selectOptionByValue(findTestObject('Reset Time Bank_PTO/Page_HRdirect/select_Select a code                       _e3a280'), 
    '2675', true)

WebUI.setText(findTestObject('Reset Time Bank_PTO/Page_HRdirect/input__datePickerComponent'), '02/09/2020')

WebUI.setText(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/input_Time_timeTaken'), '2')

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/button_Save_1'))

WebUI.delay(5)

WebUI.delay(0)

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_more_vert'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/a_Reset Starting Hours'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_more_vert'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_more_vert'))

WebUI.setText(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/input_Starting Hours_vacationStartTime'), 
    '20')

WebUI.click(findTestObject('Reset Time Bank_PTO/Page_HRdirect/input_Starting Hours_vacationStartTime'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/button_Save'))

WebUI.delay(5)

not_run: WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_keyboard_arrow_right'))

not_run: WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_keyboard_arrow_left'))

not_run: WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_keyboard_arrow_left'))

not_run: WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_keyboard_arrow_right'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_more_vert'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/a_Reset Starting Hours'))

WebUI.click(findTestObject('Reset Time Bank_PTO/Page_HRdirect/div_Additional Hours'))

WebUI.delay(3)

WebUI.click(findTestObject('Reset Time Bank_PTO/Page_HRdirect/div_Employee01 Employee01 (1)'))

WebUI.delay(2)

WebUI.click(findTestObject('Reset Time Bank_PTO/Page_HRdirect/span_Delete Event (1)'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/button_Yes'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_more_vert'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_more_vert'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/i_arrow_drop_down'))

WebUI.click(findTestObject('Object Repository/Reset Time Bank_PTO/Page_HRdirect/a_exit_to_app Sign Out'))

WebUI.closeBrowser()

