import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://account.hrdirectappsqa.com/Home/Login/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Resources - Download Blank Calendars/Page_HRdirect/input_EMAIL ADDRESS'), 
    'halebmerin+38saas19qa@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Resources - Download Blank Calendars/Page_HRdirect/input_PASSWORD'), 
    'cHBFEg1ykROLjuIVhbxYXA==')

WebUI.click(findTestObject('Object Repository/Resources - Download Blank Calendars/Page_HRdirect/button_Sign In to My Account'))

WebUI.delay(5)

'Launch Attendance Calendar app'
WebUI.click(findTestObject('Object Repository/Resources - Download Blank Calendars/Page_HRdirect/app_Calendar'))

WebUI.delay(15)

'Go to Resources tab'
WebUI.click(findTestObject('Object Repository/Resources - Download Blank Calendars/Page_HRdirect/tab_Resources'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources - Download Blank Calendars/Page_HRdirect/VacSic_2018'))

WebUI.delay(7)

WebUI.callTestCase(findTestCase('SAAS_Attendance Calendar/Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowUrl('https://calendar.hrdirectappsqa.com/acresources')

WebUI.delay(3)

WebUI.click(findTestObject('Resources - Download Blank Calendars/Page_HRdirect/VacSic_2019'))

WebUI.delay(7)

WebUI.callTestCase(findTestCase('SAAS_Attendance Calendar/Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowUrl('https://calendar.hrdirectappsqa.com/acresources')

WebUI.delay(3)

WebUI.click(findTestObject('Resources - Download Blank Calendars/Page_HRdirect/VacSic_2020'))

WebUI.delay(7)

WebUI.callTestCase(findTestCase('SAAS_Attendance Calendar/Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowUrl('https://calendar.hrdirectappsqa.com/acresources')

WebUI.delay(3)

WebUI.click(findTestObject('Resources - Download Blank Calendars/Page_HRdirect/VacSic_BlankMonth'))

WebUI.delay(3)

WebUI.callTestCase(findTestCase('SAAS_Attendance Calendar/Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowUrl('https://calendar.hrdirectappsqa.com/acresources')

WebUI.delay(3)

WebUI.click(findTestObject('Resources - Download Blank Calendars/Page_HRdirect/PTO_2018'))

WebUI.delay(3)

WebUI.callTestCase(findTestCase('SAAS_Attendance Calendar/Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Resources - Download Blank Calendars/Page_HRdirect/PTO_2019'))

WebUI.delay(3)

WebUI.callTestCase(findTestCase('SAAS_Attendance Calendar/Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowUrl('https://calendar.hrdirectappsqa.com/acresources')

WebUI.delay(3)

WebUI.click(findTestObject('Resources - Download Blank Calendars/Page_HRdirect/PTO_2020'))

WebUI.delay(3)

WebUI.callTestCase(findTestCase('SAAS_Attendance Calendar/Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowUrl('https://calendar.hrdirectappsqa.com/acresources')

WebUI.delay(3)

WebUI.click(findTestObject('Resources - Download Blank Calendars/Page_HRdirect/PTO_BlankMonth'))

WebUI.callTestCase(findTestCase('SAAS_Attendance Calendar/Download_TC'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

